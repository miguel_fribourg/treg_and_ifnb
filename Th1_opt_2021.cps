<?xml version="1.0" encoding="UTF-8"?>
<!-- generated with COPASI 4.27 (Build 217) (http://www.copasi.org) at 2021-12-14T21:55:13Z -->
<?oxygen RNGSchema="http://www.copasi.org/static/schema/CopasiML.rng" type="xml"?>
<COPASI xmlns="http://www.copasi.org/static/schema" versionMajor="4" versionMinor="27" versionDevel="217" copasiSourcesModified="0">
  <ListOfFunctions>
    <Function key="Function_13" name="Mass action (irreversible)" type="MassAction" reversible="false">
      <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
   <rdf:Description rdf:about="#Function_13">
   <CopasiMT:is rdf:resource="urn:miriam:obo.sbo:SBO:0000041" />
   </rdf:Description>
   </rdf:RDF>
      </MiriamAnnotation>
      <Comment>
        <body xmlns="http://www.w3.org/1999/xhtml">
<b>Mass action rate law for first order irreversible reactions</b>
<p>
Reaction scheme where the products are created from the reactants and the change of a product quantity is proportional to the product of reactant activities. The reaction scheme does not include any reverse process that creates the reactants from the products. The change of a product quantity is proportional to the quantity of one reactant.
</p>
</body>
      </Comment>
      <Expression>
        k1*PRODUCT&lt;substrate_i>
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_80" name="k1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_81" name="substrate" order="1" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_14" name="Mass action (reversible)" type="MassAction" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
   <rdf:Description rdf:about="#Function_14">
   <CopasiMT:is rdf:resource="urn:miriam:obo.sbo:SBO:0000042" />
   </rdf:Description>
   </rdf:RDF>
      </MiriamAnnotation>
      <Comment>
        <body xmlns="http://www.w3.org/1999/xhtml">
<b>Mass action rate law for reversible reactions</b>
<p>
Reaction scheme where the products are created from the reactants and the change of a product quantity is proportional to the product of reactant activities. The reaction scheme does include a reverse process that creates the reactants from the products.
</p>
</body>
      </Comment>
      <Expression>
        k1*PRODUCT&lt;substrate_i>-k2*PRODUCT&lt;product_j>
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_69" name="k1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_68" name="substrate" order="1" role="substrate"/>
        <ParameterDescription key="FunctionParameter_78" name="k2" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_79" name="product" order="3" role="product"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_40" name="2 Reactants, 1 Inhibitor_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_40">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*species_12*s2*(K^parameter_1/(s34^parameter_1+K^parameter_1))-Vr*s3
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_264" name="K" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_263" name="Vf" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_262" name="Vr" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_261" name="parameter_1" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_250" name="s2" order="4" role="substrate"/>
        <ParameterDescription key="FunctionParameter_265" name="s3" order="5" role="product"/>
        <ParameterDescription key="FunctionParameter_266" name="s34" order="6" role="modifier"/>
        <ParameterDescription key="FunctionParameter_267" name="species_12" order="7" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_41" name="1 Reactant, 1 Activator_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_41">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T16:02:35Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s65*(1+s3^parameter_1/(s3^parameter_1+K^parameter_1))-Vr*s10
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_275" name="K" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_274" name="Vf" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_273" name="Vr" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_272" name="parameter_1" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_271" name="s10" order="4" role="product"/>
        <ParameterDescription key="FunctionParameter_270" name="s3" order="5" role="modifier"/>
        <ParameterDescription key="FunctionParameter_269" name="s65" order="6" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_42" name="2 Reactants, 2 Inhibitors_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_42">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s11*s13*(K1^parameter_1/(s34^parameter_1+K1^parameter_1))*(K2^parameter_1/(s83^parameter_1+K2^parameter_1))-Vr*s14
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_281" name="K1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_280" name="K2" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_279" name="Vf" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_278" name="Vr" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_277" name="parameter_1" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_276" name="s11" order="5" role="substrate"/>
        <ParameterDescription key="FunctionParameter_268" name="s13" order="6" role="substrate"/>
        <ParameterDescription key="FunctionParameter_282" name="s14" order="7" role="product"/>
        <ParameterDescription key="FunctionParameter_283" name="s34" order="8" role="modifier"/>
        <ParameterDescription key="FunctionParameter_284" name="s83" order="9" role="modifier"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_43" name="2 Reactants, 2 Inhibitors, 1  Activator_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_43">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*species_10*species_11*(K1^parameter_1/(s83^parameter_1+K1^parameter_1))*(K2^parameter_1/(s40^parameter_1+K2^parameter_1))*(1+s10^parameter_1/(s10^parameter_1+K3^parameter_1))-Vr*s20
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_294" name="K1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_293" name="K2" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_292" name="K3" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_291" name="Vf" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_290" name="Vr" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_289" name="parameter_1" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_288" name="s10" order="6" role="modifier"/>
        <ParameterDescription key="FunctionParameter_287" name="s20" order="7" role="product"/>
        <ParameterDescription key="FunctionParameter_286" name="s40" order="8" role="modifier"/>
        <ParameterDescription key="FunctionParameter_285" name="s83" order="9" role="modifier"/>
        <ParameterDescription key="FunctionParameter_295" name="species_10" order="10" role="substrate"/>
        <ParameterDescription key="FunctionParameter_296" name="species_11" order="11" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_44" name="2 Reactants, 1 Inhibitor_2" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_44">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s22*s24*(K^parameter_1/(s29^parameter_1+K^parameter_1))-Vr*s25
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_308" name="K" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_307" name="Vf" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_306" name="Vr" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_305" name="parameter_1" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_304" name="s22" order="4" role="substrate"/>
        <ParameterDescription key="FunctionParameter_303" name="s24" order="5" role="substrate"/>
        <ParameterDescription key="FunctionParameter_302" name="s25" order="6" role="product"/>
        <ParameterDescription key="FunctionParameter_301" name="s29" order="7" role="modifier"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_45" name="1 Reactant, 3 Inhibitors, 3  Activators_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_45">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T15:18:29Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*species_5*(K1^parameter_1/(species_14^parameter_1+K1^parameter_1))*(K2^parameter_1/(s49^parameter_1+K2^parameter_1))*(K3^parameter_1/(s83^parameter_1+K3^parameter_1))*(s21^parameter_1/(s21^parameter_1+K4^parameter_1)+s20^parameter_1/(s20^parameter_1+K5^parameter_1)+s27^parameter_1/(s27^parameter_1+K6^parameter_1))-Vr*s68
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_312" name="K1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_311" name="K2" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_310" name="K3" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_309" name="K4" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_297" name="K5" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_298" name="K6" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_299" name="Vf" order="6" role="constant"/>
        <ParameterDescription key="FunctionParameter_300" name="Vr" order="7" role="constant"/>
        <ParameterDescription key="FunctionParameter_313" name="parameter_1" order="8" role="constant"/>
        <ParameterDescription key="FunctionParameter_314" name="s20" order="9" role="modifier"/>
        <ParameterDescription key="FunctionParameter_315" name="s21" order="10" role="modifier"/>
        <ParameterDescription key="FunctionParameter_316" name="s27" order="11" role="modifier"/>
        <ParameterDescription key="FunctionParameter_317" name="s49" order="12" role="modifier"/>
        <ParameterDescription key="FunctionParameter_318" name="s68" order="13" role="product"/>
        <ParameterDescription key="FunctionParameter_319" name="s83" order="14" role="modifier"/>
        <ParameterDescription key="FunctionParameter_320" name="species_14" order="15" role="modifier"/>
        <ParameterDescription key="FunctionParameter_321" name="species_5" order="16" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_46" name="1 Reactant, 2 Inhibitors, 3 Activators_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_46">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s80*(K1^parameter_1/(s45^parameter_1+K1^parameter_1))*(K2^parameter_1/(s49^parameter_1+K2^parameter_1))*(s21^parameter_1/(s21^parameter_1+K3^parameter_1)+s26^parameter_1/(s26^parameter_1+K4^parameter_1)+s27^parameter_1/(s27^parameter_1+K5^parameter_1))-Vr*s27
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_338" name="K1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_337" name="K2" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_336" name="K3" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_335" name="K4" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_334" name="K5" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_333" name="Vf" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_332" name="Vr" order="6" role="constant"/>
        <ParameterDescription key="FunctionParameter_331" name="parameter_1" order="7" role="constant"/>
        <ParameterDescription key="FunctionParameter_330" name="s21" order="8" role="modifier"/>
        <ParameterDescription key="FunctionParameter_329" name="s26" order="9" role="modifier"/>
        <ParameterDescription key="FunctionParameter_328" name="s27" order="10" role="product"/>
        <ParameterDescription key="FunctionParameter_327" name="s45" order="11" role="modifier"/>
        <ParameterDescription key="FunctionParameter_326" name="s49" order="12" role="modifier"/>
        <ParameterDescription key="FunctionParameter_325" name="s80" order="13" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_47" name="1 Reactant, 2 Inhibitors, 1  Activator_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_47">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s69*(K1^parameter_1/(s83^parameter_1+K1^parameter_1))*(K2^parameter_1/(s29^parameter_1+K2^parameter_1))*(1+s25^parameter_1/(s25^parameter_1+K3^parameter_1))-Vr*s28
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_349" name="K1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_348" name="K2" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_347" name="K3" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_346" name="Vf" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_345" name="Vr" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_344" name="parameter_1" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_343" name="s25" order="6" role="modifier"/>
        <ParameterDescription key="FunctionParameter_342" name="s28" order="7" role="product"/>
        <ParameterDescription key="FunctionParameter_341" name="s29" order="8" role="modifier"/>
        <ParameterDescription key="FunctionParameter_340" name="s69" order="9" role="substrate"/>
        <ParameterDescription key="FunctionParameter_339" name="s83" order="10" role="modifier"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_48" name="1 Reactant, 2 Activators_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_48">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T13:47:10Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s79*(s26^parameter_1/(s26^parameter_1+K1^parameter_1))*(s27^parameter_1/(s27^parameter_1+K2^parameter_1))-Vr*s29
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_357" name="K1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_356" name="K2" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_355" name="Vf" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_354" name="Vr" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_353" name="parameter_1" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_352" name="s26" order="5" role="modifier"/>
        <ParameterDescription key="FunctionParameter_351" name="s27" order="6" role="modifier"/>
        <ParameterDescription key="FunctionParameter_350" name="s29" order="7" role="product"/>
        <ParameterDescription key="FunctionParameter_324" name="s79" order="8" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_49" name="2 Reactants, 1 Inhibitor_3" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_49">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s30*s32*(K^parameter_1/(s29^parameter_1+K^parameter_1))-Vr*s33
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_364" name="K" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_363" name="Vf" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_362" name="Vr" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_361" name="parameter_1" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_360" name="s29" order="4" role="modifier"/>
        <ParameterDescription key="FunctionParameter_359" name="s30" order="5" role="substrate"/>
        <ParameterDescription key="FunctionParameter_358" name="s32" order="6" role="substrate"/>
        <ParameterDescription key="FunctionParameter_322" name="s33" order="7" role="product"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_50" name="1 Reactant, 2 Inhibitors, 3 Activators_2" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_50">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*species_15*(K1^parameter_1/(s26^parameter_1+K1^parameter_1))*(K2^parameter_1/(species_13^parameter_1+K2^parameter_1))*(s31^parameter_1/(s31^parameter_1+K3^parameter_1)+s35^parameter_1/(s35^parameter_1+K4^parameter_1)+s39^parameter_1/(s39^parameter_1+K5^parameter_1))-Vr*s31
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_371" name="K1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_370" name="K2" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_369" name="K3" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_368" name="K4" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_367" name="K5" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_366" name="Vf" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_365" name="Vr" order="6" role="constant"/>
        <ParameterDescription key="FunctionParameter_323" name="parameter_1" order="7" role="constant"/>
        <ParameterDescription key="FunctionParameter_372" name="s26" order="8" role="modifier"/>
        <ParameterDescription key="FunctionParameter_373" name="s31" order="9" role="product"/>
        <ParameterDescription key="FunctionParameter_374" name="s35" order="10" role="modifier"/>
        <ParameterDescription key="FunctionParameter_375" name="s39" order="11" role="modifier"/>
        <ParameterDescription key="FunctionParameter_376" name="species_13" order="12" role="modifier"/>
        <ParameterDescription key="FunctionParameter_377" name="species_15" order="13" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_51" name="1 Reactant, 3 Inhibitors, 2  Activators_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_51">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s81*(K1^parameter_1/(s27^parameter_1+K1^parameter_1))*(K2^parameter_1/(s49^parameter_1+K2^parameter_1))*(K3^parameter_1/(s45^parameter_1+K3^parameter_1))*(s34^parameter_1/(s34^parameter_1+K4^parameter_1)+s83^parameter_1/(s83^parameter_1+K5^parameter_1))-Vr*s35
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_391" name="K1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_390" name="K2" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_389" name="K3" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_388" name="K4" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_387" name="K5" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_386" name="Vf" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_385" name="Vr" order="6" role="constant"/>
        <ParameterDescription key="FunctionParameter_384" name="parameter_1" order="7" role="constant"/>
        <ParameterDescription key="FunctionParameter_383" name="s27" order="8" role="modifier"/>
        <ParameterDescription key="FunctionParameter_382" name="s34" order="9" role="modifier"/>
        <ParameterDescription key="FunctionParameter_381" name="s35" order="10" role="product"/>
        <ParameterDescription key="FunctionParameter_380" name="s45" order="11" role="modifier"/>
        <ParameterDescription key="FunctionParameter_379" name="s49" order="12" role="modifier"/>
        <ParameterDescription key="FunctionParameter_378" name="s81" order="13" role="substrate"/>
        <ParameterDescription key="FunctionParameter_392" name="s83" order="14" role="modifier"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_52" name="1 Reactant, 3 Inhibitors, 4  Activators_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_52">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s78*(K1^parameter_1/(s34^parameter_1+K1^parameter_1))*(K2^parameter_1/(s49^parameter_1+K2^parameter_1))*(K3^parameter_1/(s48^parameter_1+K3^parameter_1))*(s45^parameter_1/(s45^parameter_1+K4^parameter_1)+s26^parameter_1/(s26^parameter_1+K5^parameter_1)+s39^parameter_1/(s39^parameter_1+K6^parameter_1)+s83^parameter_1/(s83^parameter_1+K7^parameter_1))-Vr*s40
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_407" name="K1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_406" name="K2" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_405" name="K3" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_404" name="K4" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_403" name="K5" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_402" name="K6" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_401" name="K7" order="6" role="constant"/>
        <ParameterDescription key="FunctionParameter_400" name="Vf" order="7" role="constant"/>
        <ParameterDescription key="FunctionParameter_399" name="Vr" order="8" role="constant"/>
        <ParameterDescription key="FunctionParameter_398" name="parameter_1" order="9" role="constant"/>
        <ParameterDescription key="FunctionParameter_397" name="s26" order="10" role="modifier"/>
        <ParameterDescription key="FunctionParameter_396" name="s34" order="11" role="modifier"/>
        <ParameterDescription key="FunctionParameter_395" name="s39" order="12" role="modifier"/>
        <ParameterDescription key="FunctionParameter_394" name="s40" order="13" role="product"/>
        <ParameterDescription key="FunctionParameter_393" name="s45" order="14" role="modifier"/>
        <ParameterDescription key="FunctionParameter_408" name="s48" order="15" role="modifier"/>
        <ParameterDescription key="FunctionParameter_409" name="s49" order="16" role="modifier"/>
        <ParameterDescription key="FunctionParameter_410" name="s78" order="17" role="substrate"/>
        <ParameterDescription key="FunctionParameter_411" name="s83" order="18" role="modifier"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_53" name="1 Reactant, 2 Inhibitors, 2  Activators_2" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_53">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s75*(K1^parameter_1/(s40^parameter_1+K1^parameter_1))*(K2^parameter_1/(s83^parameter_1+K2^parameter_1))*(s49^parameter_1/(s49^parameter_1+K3^parameter_1)+s45^parameter_1/(s45^parameter_1+K4^parameter_1))-Vr*s50
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_430" name="K1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_429" name="K2" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_428" name="K3" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_427" name="K4" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_426" name="Vf" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_425" name="Vr" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_424" name="parameter_1" order="6" role="constant"/>
        <ParameterDescription key="FunctionParameter_423" name="s40" order="7" role="modifier"/>
        <ParameterDescription key="FunctionParameter_422" name="s45" order="8" role="modifier"/>
        <ParameterDescription key="FunctionParameter_421" name="s49" order="9" role="modifier"/>
        <ParameterDescription key="FunctionParameter_420" name="s50" order="10" role="product"/>
        <ParameterDescription key="FunctionParameter_419" name="s75" order="11" role="substrate"/>
        <ParameterDescription key="FunctionParameter_418" name="s83" order="12" role="modifier"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_54" name="2 Reactants, 1 Inhibitor, 1  Activator_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_54">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s51*s53*(K^parameter_1/(s39^parameter_1+K^parameter_1))*(1+species_3^parameter_1/(species_3^parameter_1+K1^parameter_1))-Vr*s54
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_437" name="K" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_436" name="K1" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_435" name="Vf" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_434" name="Vr" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_433" name="parameter_1" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_432" name="s39" order="5" role="modifier"/>
        <ParameterDescription key="FunctionParameter_431" name="s51" order="6" role="substrate"/>
        <ParameterDescription key="FunctionParameter_412" name="s53" order="7" role="substrate"/>
        <ParameterDescription key="FunctionParameter_413" name="s54" order="8" role="product"/>
        <ParameterDescription key="FunctionParameter_414" name="species_3" order="9" role="modifier"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_55" name="2 Reactants, 1 Inhibitor_4" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_55">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s57*s58*(K^parameter_1/(s27^parameter_1+K^parameter_1))-Vr*s59
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_444" name="K" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_443" name="Vf" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_442" name="Vr" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_441" name="parameter_1" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_440" name="s27" order="4" role="modifier"/>
        <ParameterDescription key="FunctionParameter_439" name="s57" order="5" role="substrate"/>
        <ParameterDescription key="FunctionParameter_438" name="s58" order="6" role="substrate"/>
        <ParameterDescription key="FunctionParameter_417" name="s59" order="7" role="product"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_56" name="2 Reactants, 1 Inhibitor, 1  Activator_2" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_56">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*species_9*species_8*(K^parameter_1/(s83^parameter_1+K^parameter_1))*(1+s48^parameter_1/(s48^parameter_1+K1^parameter_1))-Vr*s57
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_450" name="K" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_449" name="K1" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_448" name="Vf" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_447" name="Vr" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_446" name="parameter_1" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_445" name="s48" order="5" role="modifier"/>
        <ParameterDescription key="FunctionParameter_415" name="s57" order="6" role="product"/>
        <ParameterDescription key="FunctionParameter_416" name="s83" order="7" role="modifier"/>
        <ParameterDescription key="FunctionParameter_451" name="species_8" order="8" role="substrate"/>
        <ParameterDescription key="FunctionParameter_452" name="species_9" order="9" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_57" name="1 Reactant, 1 Activator_3" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_57">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T13:46:55Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*species_6*(1+s49^parameter_1/(s49^parameter_1+K^parameter_1))-Vr*s52
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_462" name="K" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_461" name="Vf" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_460" name="Vr" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_459" name="parameter_1" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_458" name="s49" order="4" role="modifier"/>
        <ParameterDescription key="FunctionParameter_457" name="s52" order="5" role="product"/>
        <ParameterDescription key="FunctionParameter_456" name="species_6" order="6" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_58" name="2 Reactants, 1 Inhibitor, 1  Activator_3" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_58">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s85*s86*(K^parameter_1/(s25^parameter_1+K^parameter_1))*(1+s33^parameter_1/(s33^parameter_1+K1^parameter_1))-Vr*s83
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_466" name="K" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_465" name="K1" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_464" name="Vf" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_463" name="Vr" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_453" name="parameter_1" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_454" name="s25" order="5" role="modifier"/>
        <ParameterDescription key="FunctionParameter_455" name="s33" order="6" role="modifier"/>
        <ParameterDescription key="FunctionParameter_467" name="s83" order="7" role="product"/>
        <ParameterDescription key="FunctionParameter_468" name="s85" order="8" role="substrate"/>
        <ParameterDescription key="FunctionParameter_469" name="s86" order="9" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_59" name="1 Reactant, 1 Inhibitor, 3  Activators_2" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_59">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T15:44:55Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*species_7*(K^parameter_1/(s83^parameter_1+K^parameter_1))*(s59^parameter_1/(s59^parameter_1+K1^parameter_1)+s50^parameter_1/(s50^parameter_1+K2^parameter_1)+s21^parameter_1/(s21^parameter_1+K3^parameter_1))-Vr*s73
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_479" name="K" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_478" name="K1" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_477" name="K2" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_476" name="K3" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_475" name="Vf" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_474" name="Vr" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_473" name="parameter_1" order="6" role="constant"/>
        <ParameterDescription key="FunctionParameter_472" name="s21" order="7" role="modifier"/>
        <ParameterDescription key="FunctionParameter_471" name="s50" order="8" role="modifier"/>
        <ParameterDescription key="FunctionParameter_470" name="s59" order="9" role="modifier"/>
        <ParameterDescription key="FunctionParameter_480" name="s73" order="10" role="product"/>
        <ParameterDescription key="FunctionParameter_481" name="s83" order="11" role="modifier"/>
        <ParameterDescription key="FunctionParameter_482" name="species_7" order="12" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_60" name="Pool coupling_1" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_60">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        V*(species_16^parameter_1/(species_16^parameter_1+species_12^parameter_1+0.001)-k*species_12)
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_495" name="V" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_494" name="k" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_493" name="parameter_1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_492" name="species_12" order="3" role="product"/>
        <ParameterDescription key="FunctionParameter_491" name="species_16" order="4" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_61" name="Pool coupling_2" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_61">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T16:03:55Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        V*(species_17^parameter_1/(species_17^parameter_1+s11^parameter_1+0.001)-k*s11)
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_486" name="V" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_487" name="k" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_488" name="parameter_1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_489" name="s11" order="3" role="product"/>
        <ParameterDescription key="FunctionParameter_490" name="species_17" order="4" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_62" name="Pool coupling_3" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_62">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        V*(species_18^parameter_1/(species_18^parameter_1+s22^parameter_1+0.001)-k*s22)
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_497" name="V" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_496" name="k" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_483" name="parameter_1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_484" name="s22" order="3" role="product"/>
        <ParameterDescription key="FunctionParameter_485" name="species_18" order="4" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_63" name="Pool coupling_4" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_63">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        V*(species_19^parameter_1/(species_19^parameter_1+s51^parameter_1+0.001)-k*s51)
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_502" name="V" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_501" name="k" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_500" name="parameter_1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_499" name="s51" order="3" role="product"/>
        <ParameterDescription key="FunctionParameter_498" name="species_19" order="4" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_64" name="Pool coupling_5" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_64">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        V*(species_20^parameter_1/(species_20^parameter_1+s55^parameter_1+0.001)-k*s55)
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_507" name="V" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_506" name="k" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_505" name="parameter_1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_504" name="s55" order="3" role="product"/>
        <ParameterDescription key="FunctionParameter_503" name="species_20" order="4" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_65" name="Pool coupling_6" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_65">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        V*(species_21^parameter_1/(species_21^parameter_1+s73^parameter_1+0.001)-k*s73)
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_512" name="V" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_511" name="k" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_510" name="parameter_1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_509" name="s73" order="3" role="product"/>
        <ParameterDescription key="FunctionParameter_508" name="species_21" order="4" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_66" name="Pool coupling_7" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_66">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        V*(species_22^parameter_1/(species_22^parameter_1+species_2^parameter_1+0.001)-k*species_2)
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_517" name="V" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_516" name="k" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_515" name="parameter_1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_514" name="species_2" order="3" role="product"/>
        <ParameterDescription key="FunctionParameter_513" name="species_22" order="4" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_67" name="Pool coupling_8" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_67">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        V*(species_23^parameter_1/(species_23^parameter_1+s90^parameter_1+0.001)-k*s90)
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_522" name="V" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_521" name="k" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_520" name="parameter_1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_519" name="s90" order="3" role="product"/>
        <ParameterDescription key="FunctionParameter_518" name="species_23" order="4" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_68" name="Pool coupling_9" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_68">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        V*(species_24^parameter_1/(species_24^parameter_1+s89^parameter_1+0.001)-k*s89)
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_527" name="V" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_526" name="k" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_525" name="parameter_1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_524" name="s89" order="3" role="product"/>
        <ParameterDescription key="FunctionParameter_523" name="species_24" order="4" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_69" name="Pool coupling_10" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_69">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        V*(species_25^parameter_1/(species_25^parameter_1+s87^parameter_1+0.001)-k*s87)
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_532" name="V" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_531" name="k" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_530" name="parameter_1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_529" name="s87" order="3" role="product"/>
        <ParameterDescription key="FunctionParameter_528" name="species_25" order="4" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_70" name="Pool coupling_11" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Function_70">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2020-05-02T00:31:31Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        V*(species_26^parameter_1/(species_26^parameter_1+s30^parameter_1+0.001)-k*s30)
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_537" name="V" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_536" name="k" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_535" name="parameter_1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_534" name="s30" order="3" role="product"/>
        <ParameterDescription key="FunctionParameter_533" name="species_26" order="4" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_71" name="1 Reactant, 1 Inhibitor, 4  Activators_2" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_71">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T15:44:59Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*species_27*(K^parameter_1/(s59^parameter_1+K^parameter_1))*(s83^parameter_1/(s83^parameter_1+K1^parameter_1)+s54^parameter_1/(s54^parameter_1+K2^parameter_1)+s35^parameter_1/(s35^parameter_1+K3^parameter_1)+s39^parameter_1/(s39^parameter_1+K4^parameter_1))-Vr*s74
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_542" name="K" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_541" name="K1" order="1" role="constant"/>
        <ParameterDescription key="FunctionParameter_540" name="K2" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_539" name="K3" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_538" name="K4" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_543" name="Vf" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_544" name="Vr" order="6" role="constant"/>
        <ParameterDescription key="FunctionParameter_545" name="parameter_1" order="7" role="constant"/>
        <ParameterDescription key="FunctionParameter_546" name="s35" order="8" role="modifier"/>
        <ParameterDescription key="FunctionParameter_547" name="s39" order="9" role="modifier"/>
        <ParameterDescription key="FunctionParameter_548" name="s54" order="10" role="modifier"/>
        <ParameterDescription key="FunctionParameter_549" name="s59" order="11" role="modifier"/>
        <ParameterDescription key="FunctionParameter_550" name="s74" order="12" role="product"/>
        <ParameterDescription key="FunctionParameter_551" name="s83" order="13" role="modifier"/>
        <ParameterDescription key="FunctionParameter_552" name="species_27" order="14" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_72" name="1 Reactant, 1 Inhibitor, 5 Activators, Coupled" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_72">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T15:04:31Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s76*(K^parameter_1/(s83^parameter_1+K^parameter_1))*(s54^parameter_1/(s54^parameter_1+K1^parameter_1)+s59^parameter_1/(s59^parameter_1+K2^parameter_1)+s63^parameter_1/(s63^parameter_1+K3^parameter_1)+s48^parameter_1/(s48^parameter_1+K4^parameter_1)+alpha3*(s50^parameter_1/(s50^parameter_1+K5^parameter_1)))-Vr*s49
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_567" name="Vf" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_566" name="s76" order="1" role="substrate"/>
        <ParameterDescription key="FunctionParameter_565" name="K" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_564" name="parameter_1" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_563" name="s83" order="4" role="modifier"/>
        <ParameterDescription key="FunctionParameter_562" name="s54" order="5" role="modifier"/>
        <ParameterDescription key="FunctionParameter_561" name="K1" order="6" role="constant"/>
        <ParameterDescription key="FunctionParameter_560" name="s59" order="7" role="modifier"/>
        <ParameterDescription key="FunctionParameter_559" name="K2" order="8" role="constant"/>
        <ParameterDescription key="FunctionParameter_558" name="s63" order="9" role="modifier"/>
        <ParameterDescription key="FunctionParameter_557" name="K3" order="10" role="constant"/>
        <ParameterDescription key="FunctionParameter_556" name="s48" order="11" role="modifier"/>
        <ParameterDescription key="FunctionParameter_555" name="K4" order="12" role="constant"/>
        <ParameterDescription key="FunctionParameter_554" name="s50" order="13" role="modifier"/>
        <ParameterDescription key="FunctionParameter_553" name="Vr" order="14" role="constant"/>
        <ParameterDescription key="FunctionParameter_568" name="s49" order="15" role="product"/>
        <ParameterDescription key="FunctionParameter_569" name="K5" order="16" role="constant"/>
        <ParameterDescription key="FunctionParameter_570" name="alpha3" order="17" role="constant"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_73" name="1 Reactant, 1 Inhibitor, 4 Activators, Coupled" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_73">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T15:44:59Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*species_27*(K^parameter_1/(s59^parameter_1+K^parameter_1))*(s83^parameter_1/(s83^parameter_1+K1^parameter_1)+s54^parameter_1/(s54^parameter_1+K2^parameter_1)+s35^parameter_1/(s35^parameter_1+K3^parameter_1)+alpha1*(s39^parameter_1/(s39^parameter_1+K4^parameter_1)))-Vr*s74
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_588" name="Vf" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_587" name="species_27" order="1" role="substrate"/>
        <ParameterDescription key="FunctionParameter_586" name="K" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_585" name="parameter_1" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_584" name="s59" order="4" role="modifier"/>
        <ParameterDescription key="FunctionParameter_583" name="s83" order="5" role="modifier"/>
        <ParameterDescription key="FunctionParameter_582" name="K1" order="6" role="constant"/>
        <ParameterDescription key="FunctionParameter_581" name="s54" order="7" role="modifier"/>
        <ParameterDescription key="FunctionParameter_580" name="K2" order="8" role="constant"/>
        <ParameterDescription key="FunctionParameter_579" name="s35" order="9" role="modifier"/>
        <ParameterDescription key="FunctionParameter_578" name="K3" order="10" role="constant"/>
        <ParameterDescription key="FunctionParameter_577" name="s39" order="11" role="modifier"/>
        <ParameterDescription key="FunctionParameter_576" name="K4" order="12" role="constant"/>
        <ParameterDescription key="FunctionParameter_575" name="Vr" order="13" role="constant"/>
        <ParameterDescription key="FunctionParameter_574" name="s74" order="14" role="product"/>
        <ParameterDescription key="FunctionParameter_573" name="alpha1" order="15" role="constant"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_74" name="1 Reactant 1 2 Activators Coupled" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_74">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T13:47:10Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s79*((s26^parameter_1/(s26^parameter_1+K1^parameter_1))+alpha5*(s27^parameter_1/(s27^parameter_1+K2^parameter_1)))-Vr*s29
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_602" name="Vf" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_601" name="s79" order="1" role="substrate"/>
        <ParameterDescription key="FunctionParameter_600" name="s26" order="2" role="modifier"/>
        <ParameterDescription key="FunctionParameter_599" name="parameter_1" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_598" name="K1" order="4" role="constant"/>
        <ParameterDescription key="FunctionParameter_597" name="alpha5" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_596" name="s27" order="6" role="modifier"/>
        <ParameterDescription key="FunctionParameter_595" name="K2" order="7" role="constant"/>
        <ParameterDescription key="FunctionParameter_594" name="Vr" order="8" role="constant"/>
        <ParameterDescription key="FunctionParameter_593" name="s29" order="9" role="product"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_75" name="1 reactant, 2 inhibitor, 3 activators, Coupled" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_75">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T17:37:40Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s67*(K1^parameter_1/(s54^parameter_1+K1^parameter_1))*(K2^parameter_1/(s35^parameter_1+K2^parameter_1))*(s14^parameter_1/(s14^parameter_1+K3^parameter_1)+s59^parameter_1/(s59^parameter_1+K4^parameter_1)+alpha4*(s60^parameter_1/(s60^parameter_1+K5^parameter_1)))-Vr*s21
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_606" name="Vf" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_605" name="s67" order="1" role="substrate"/>
        <ParameterDescription key="FunctionParameter_604" name="K1" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_603" name="parameter_1" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_572" name="s54" order="4" role="modifier"/>
        <ParameterDescription key="FunctionParameter_571" name="K2" order="5" role="constant"/>
        <ParameterDescription key="FunctionParameter_589" name="s35" order="6" role="modifier"/>
        <ParameterDescription key="FunctionParameter_590" name="s14" order="7" role="modifier"/>
        <ParameterDescription key="FunctionParameter_591" name="K3" order="8" role="constant"/>
        <ParameterDescription key="FunctionParameter_592" name="s59" order="9" role="modifier"/>
        <ParameterDescription key="FunctionParameter_607" name="K4" order="10" role="constant"/>
        <ParameterDescription key="FunctionParameter_608" name="alpha4" order="11" role="constant"/>
        <ParameterDescription key="FunctionParameter_609" name="s60" order="12" role="modifier"/>
        <ParameterDescription key="FunctionParameter_610" name="K5" order="13" role="constant"/>
        <ParameterDescription key="FunctionParameter_611" name="Vr" order="14" role="constant"/>
        <ParameterDescription key="FunctionParameter_612" name="s21" order="15" role="product"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_76" name="1 Reactant, 1 Inhibitor, 2 Activators, Coupled" type="UserDefined" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Function_76">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T17:49:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Vf*s82*(K^parameter_1/(s25^parameter_1+K^parameter_1))*(alpha6*(s40^paramerter_1/(s40^parameter_1+K2^parameter_1))+(s33^n1/(s33^n1+K1^n1)))-Vr*s34
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_628" name="Vf" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_627" name="s82" order="1" role="substrate"/>
        <ParameterDescription key="FunctionParameter_626" name="K" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_625" name="parameter_1" order="3" role="constant"/>
        <ParameterDescription key="FunctionParameter_624" name="s25" order="4" role="modifier"/>
        <ParameterDescription key="FunctionParameter_623" name="s33" order="5" role="modifier"/>
        <ParameterDescription key="FunctionParameter_622" name="n1" order="6" role="constant"/>
        <ParameterDescription key="FunctionParameter_621" name="K1" order="7" role="constant"/>
        <ParameterDescription key="FunctionParameter_620" name="Vr" order="8" role="constant"/>
        <ParameterDescription key="FunctionParameter_619" name="s34" order="9" role="product"/>
        <ParameterDescription key="FunctionParameter_618" name="alpha6" order="10" role="constant"/>
        <ParameterDescription key="FunctionParameter_617" name="s40" order="11" role="modifier"/>
        <ParameterDescription key="FunctionParameter_616" name="paramerter_1" order="12" role="constant"/>
        <ParameterDescription key="FunctionParameter_615" name="K2" order="13" role="constant"/>
      </ListOfParameterDescriptions>
    </Function>
  </ListOfFunctions>
  <Model key="Model_1" name="Maximized effects of IFNbeta on TH1 polarization" simulationType="time" timeUnit="h" volumeUnit="l" areaUnit="m²" lengthUnit="m" quantityUnit="mol" type="deterministic" avogadroConstant="6.0221417899999999e+23">
    <MiriamAnnotation>
<rdf:RDF
   xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#"
   xmlns:bqbiol="http://biomodels.net/biology-qualifiers/"
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:vCard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <rdf:Description rdf:about="#Model_1">
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:mamo:MAMO_0000046"/>
      </rdf:Bag>
    </bqbiol:hasProperty>
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:doid:DOID%3A104"/>
      </rdf:Bag>
    </bqbiol:hasProperty>
    <bqbiol:hasTaxon>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:taxonomy:10090"/>
      </rdf:Bag>
    </bqbiol:hasTaxon>
    <dcterms:bibliographicCitation>
      <rdf:Bag>
        <rdf:li>
          <rdf:Description>
            <CopasiMT:isDescribedBy rdf:resource="urn:miriam:pubmed:23592971"/>
          </rdf:Description>
        </rdf:li>
      </rdf:Bag>
    </dcterms:bibliographicCitation>
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2013-05-09T14:36:48Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <dcterms:creator>
      <rdf:Bag>
        <rdf:li>
          <rdf:Description>
            <vCard:EMAIL>acarbo@vbi.vt.edu</vCard:EMAIL>
            <vCard:N>
              <rdf:Description>
                <vCard:Family>Carbo</vCard:Family>
                <vCard:Given>Adria</vCard:Given>
              </rdf:Description>
            </vCard:N>
            <vCard:ORG>
              <rdf:Description>
                <vCard:Orgname>Center for Modeling Immunity to Enteric Pathogens</vCard:Orgname>
              </rdf:Description>
            </vCard:ORG>
          </rdf:Description>
        </rdf:li>
        <rdf:li>
          <rdf:Description>
            <vCard:EMAIL>viji@ebi.ac.uk</vCard:EMAIL>
            <vCard:N>
              <rdf:Description>
                <vCard:Family>Chelliah</vCard:Family>
                <vCard:Given>Vijayalakshmi</vCard:Given>
              </rdf:Description>
            </vCard:N>
            <vCard:ORG>
              <rdf:Description>
                <vCard:Orgname>EMBL-EBI</vCard:Orgname>
              </rdf:Description>
            </vCard:ORG>
          </rdf:Description>
        </rdf:li>
      </rdf:Bag>
    </dcterms:creator>
    <dcterms:modified>
      <rdf:Description>
        <dcterms:W3CDTF>2014-10-10T11:12:39Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:modified>
    <CopasiMT:is>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:biomodels.db:MODEL1304230001"/>
      </rdf:Bag>
    </CopasiMT:is>
    <CopasiMT:is>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:biomodels.db:BIOMD0000000451"/>
      </rdf:Bag>
    </CopasiMT:is>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:go:GO%3A0030217"/>
        <rdf:li rdf:resource="urn:miriam:go:GO%3A0045222"/>
      </rdf:Bag>
    </CopasiMT:isVersionOf>
    <CopasiMT:occursIn>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:bto:BTO%3A0000545"/>
      </rdf:Bag>
    </CopasiMT:occursIn>
  </rdf:Description>
</rdf:RDF>

    </MiriamAnnotation>
    <Comment>
      1.   Model Creation

The computational model developed for this work constitutes an expansion of a publicly available (https://www.ebi.ac.uk/biomodels/BIOMD0000000451) ordinary differential equation (ODE) model developed at the Virginia Bioinformatics Institute. The model includes 60 differential equations representing 52 reactions and 93 species and was implemented and simulated in COPASI. 

Model assumptions. The most relevant assumptions for the use of the model in this work are the following:
	The model assumes correct engagement of the T-cell receptor (TCR) (signal 1) and co-stimulatory receptors (signal 2). In that sense, the model is designed to explore the effects of different cytokine inputs (signal 3) on the T cells.
	The model does not explicit describe/include T cell proliferation, but rather describes the system as one stereotypical cell with varying concentrations of species, Tbet, GATA-3, RORgt, and Foxp3, which can successfully be mapped to the frequencies of different subsets T cell subsets (TH1, TH2, TH17, and TREG).  

Model expansion strategy. In order to expand the model to include the effects of interferon-beta (IFNbeta) on T cell polarization we introduced two additional species: the concentration of IFNAR (type I interferon receptor in the membrane), and the concentration of IFNbeta as one of the cytokines in the environment as a new input to the cell.  
It has been shown that type I interferons can signal through phosphorylation of different STATs depending on cell type and context. In order to make the model flexible, we allowed the ligated type I interferon receptor (IFNAR/IFNbeta) to phosphorylate STAT1, STAT3, STAT4, STAT5, and STAT6 through the control of 5 coupling constants, alpha1,alpha3,alpha4,alpha5, and alpha6, respectively: This results in the addition to the model of:  

	An ODE that describes the binding of the IFNbeta in the environment to the IFNAR receptor.
	An activation term in the ODEs that governs STAT1, STAT3, STAT4, STAT5, and STAT6  


2.   Optimization of IFNbeta effects on TH1, TH2, TH17 and TREG polarizations

We verified that, in the absence of IFNbeta the expanded model adequately polarized CD4+ naïve T cells to TH1 (in the presence of IL-12, IL-18 and IFNbeta), TH2 (in the presence of IL-4 and IL-2), TH17 (in the presence of IL-6 and TGFbeta) and TREG (in the presence of IL-2 and TGFbeta). We tested in silico the potential effects of IFNbeta on each of these polarizations by allowing the coupling (weights) of IFNAR to the different STATs to vary in the model such that they maximized the effect of IFNbeta on each of the polarizations. To achieve this we found the vector of STAT-coupling weights that maximized the overall concentration of the active form of the appropriate transcription factor (Tbet for TH1, GATA-3 for TH2, RORgt for Th17, and Foxp3 for TREG) with respect to the others in the span of 5 days (120 hour). For the parameter vector Alpha=[alpha1,alpha3,alpha4,alpha5,alpha6] with values constrained to αi∈[0,1] and initial values=0.5. We solved these highly non-linear problems using a random search algorithm implemented with 1000 iterations (in average ~500 iterations were enough to reach convergence). Optimization was run 10 times for each polarization and the optimized models for each polarization parametrized with the average values for alpha1, alpha3, alpha4, alpha5, and alpha6 of the 10 runs.
    </Comment>
    <ListOfCompartments>
      <Compartment key="Compartment_0" name="Environment" simulationType="fixed" dimensionality="3" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Compartment_0">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-05T15:59:28Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:go:GO%3A0005623" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Compartment>
      <Compartment key="Compartment_1" name="Naive T Cells" simulationType="fixed" dimensionality="3" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Compartment_1">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-05T15:57:56Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:bto:BTO%3A0002417" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Compartment>
    </ListOfCompartments>
    <ListOfMetabolites>
      <Metabolite key="Metabolite_0" name="eIFNg" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_0">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T12:43:16Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P01580" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <InitialExpression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IFNg_pool],Reference=InitialConcentration>
        </InitialExpression>
      </Metabolite>
      <Metabolite key="Metabolite_1" name="eIL12" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_1">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P43431" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P43432" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <InitialExpression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL12_pool],Reference=InitialConcentration>
        </InitialExpression>
      </Metabolite>
      <Metabolite key="Metabolite_2" name="eIL21" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_2">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9ES17" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <InitialExpression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL21_pool],Reference=InitialConcentration>
        </InitialExpression>
      </Metabolite>
      <Metabolite key="Metabolite_3" name="eIL23" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_3">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9EQ14" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <InitialExpression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL23_pool],Reference=InitialConcentration>
        </InitialExpression>
      </Metabolite>
      <Metabolite key="Metabolite_4" name="eIL4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_4">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P07750" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <InitialExpression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL4_pool],Reference=InitialConcentration>
        </InitialExpression>
      </Metabolite>
      <Metabolite key="Metabolite_5" name="eTGFb" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_5">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P04202" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <InitialExpression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[TGFb_pool],Reference=InitialConcentration>
        </InitialExpression>
      </Metabolite>
      <Metabolite key="Metabolite_6" name="eIL2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_6">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P04351" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <InitialExpression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL2_pool],Reference=InitialConcentration>
        </InitialExpression>
      </Metabolite>
      <Metabolite key="Metabolite_7" name="eIL6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_7">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T14:49:28Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P08505" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <InitialExpression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL6_pool],Reference=InitialConcentration>
        </InitialExpression>
      </Metabolite>
      <Metabolite key="Metabolite_8" name="eIL17" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_8">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T15:33:39Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:interpro:IPR010345" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <InitialExpression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL17_pool],Reference=InitialConcentration>
        </InitialExpression>
      </Metabolite>
      <Metabolite key="Metabolite_9" name="eIL10" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_9">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P18893" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <InitialExpression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL10_pool],Reference=InitialConcentration>
        </InitialExpression>
      </Metabolite>
      <Metabolite key="Metabolite_10" name="eIL18" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_10">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P70380" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <InitialExpression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL18_pool],Reference=InitialConcentration>
        </InitialExpression>
      </Metabolite>
      <Metabolite key="Metabolite_11" name="anti-IL4" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_11">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:fma:FMA%3A62871" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_12" name="anti-IFNg" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_12">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:fma:FMA%3A62871" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_13" name="pIL4" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_13">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P07750" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_14" name="IL18_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_14">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T14:51:25Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P70380" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_15" name="IL12_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_15">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P43431" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P43432" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_16" name="IFNg_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_16">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P01580" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_17" name="IL21_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_17">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9ES17" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_18" name="IL23_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_18">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9EQ14" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_19" name="IL17_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_19">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:interpro:IPR010345" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_20" name="IL10_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_20">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T14:48:26Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P18893" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_21" name="IL6_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_21">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P08505" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_22" name="IL2_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_22">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P04351" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_23" name="TGFb_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_23">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P04202" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_24" name="IL4_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_24">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P07750" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_25" name="pIL10" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_25">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P18893" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_26" name="eIFNbeta" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_26">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T13:55:31Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_27" name="IFNbeta_pool" simulationType="fixed" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_27">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T14:50:48Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_28" name="IL6-IL6R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_28">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P08505" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P22272" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q00560" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_29" name="IL6R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_29">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P22272" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q00560" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_30" name="IL6" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_30">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P08505" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_31" name="TGFb-TGFbR" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_31">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P04202" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q64729" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_32" name="TGFbR" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_32">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q64729" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_33" name="TGFb" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_33">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P04202" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_34" name="IL2-IL2R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_34">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P01590" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P04351" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_35" name="IL2R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_35">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P01590" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_36" name="IL2" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_36">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P04351" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_37" name="IL4-IL4R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_37">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P07750" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P16382" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_38" name="IL4R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_38">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P16382" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_39" name="IL4" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_39">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P07750" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_40" name="IFNg-IFNgR" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_40">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T12:58:39Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P01580" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P15261" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_41" name="IFNgR" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_41">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P15261" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_42" name="IL12-IL12R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_42">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P43431" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P43432" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P97378" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q60837" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_43" name="IL12R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_43">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P97378" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q60837" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_44" name="IL12" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_44">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P43431" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P43432" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_45" name="IL18-IL18R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_45">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P70380" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q61098" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_46" name="IL18R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_46">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q61098" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_47" name="IL18" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_47">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P70380" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_48" name="IL21" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_48">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9ES17" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_49" name="IL21-IL21R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_49">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9ES17" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9JHX3" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_50" name="IL21R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_50">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9JHX3" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_51" name="IL23R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_51">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q8K4B4" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_52" name="IL23-IL23R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_52">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q8K4B4" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9EQ14" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_53" name="IL10R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_53">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q61190" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q61727" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_54" name="IL10-IL10R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_54">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P18893" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q61190" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q61727" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_55" name="IRAK1" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_55">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q62406" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_56" name="IRAK1-P" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:bqbiol="http://biomodels.net/biology-qualifiers/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_56">
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:pato:PATO%3A0002220" />
      </rdf:Bag>
    </bqbiol:hasProperty>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q62406" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_57" name="p50/p65 dimer" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_57">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P25799" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_58" name="STAT4" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_58">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T12:53:57Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P42228" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_59" name="STAT4-P" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:bqbiol="http://biomodels.net/biology-qualifiers/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_59">
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:pato:PATO%3A0002220" />
      </rdf:Bag>
    </bqbiol:hasProperty>
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T14:58:03Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P42228" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_60" name="IFNg" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_60">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T13:03:17Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P01580" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_61" name="JAK1" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_61">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P52332" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_62" name="JAK1-P" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:bqbiol="http://biomodels.net/biology-qualifiers/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_62">
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:pato:PATO%3A0002220" />
      </rdf:Bag>
    </bqbiol:hasProperty>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P52332" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_63" name="STAT1" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_63">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T14:57:31Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P42225" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_64" name="STAT1-P" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:bqbiol="http://biomodels.net/biology-qualifiers/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_64">
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:pato:PATO%3A0002220" />
      </rdf:Bag>
    </bqbiol:hasProperty>
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T12:53:40Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P42225" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_65" name="p40/p19 dimer" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_65">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T14:57:20Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P43432" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9EQ14" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_66" name="IL17" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_66">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:interpro:IPR010345" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_67" name="IL10" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_67">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P18893" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_68" name="RORgt" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_68">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P51450" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_69" name="RORgt-ligand" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_69">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:biomodels.sbo:SBO%3A0000280" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P51450" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_70" name="STAT3-P" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:bqbiol="http://biomodels.net/biology-qualifiers/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_70">
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:pato:PATO%3A0002220" />
      </rdf:Bag>
    </bqbiol:hasProperty>
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T12:53:56Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P42227" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_71" name="STAT3" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_71">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T12:53:55Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P42227" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_72" name="STAT5-P" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:bqbiol="http://biomodels.net/biology-qualifiers/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_72">
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:pato:PATO%3A0002220" />
      </rdf:Bag>
    </bqbiol:hasProperty>
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T14:58:14Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P42230" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_73" name="STAT5" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_73">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P42230" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_74" name="FOXP3" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_74">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q99JB6" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_75" name="SOCS1" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_75">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:O35716" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_76" name="SOCS1-JAKs" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_76">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:O35716" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P52332" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_77" name="Tbet-P" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:bqbiol="http://biomodels.net/biology-qualifiers/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_77">
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:pato:PATO%3A0002220" />
      </rdf:Bag>
    </bqbiol:hasProperty>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q5PSB0" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_78" name="Tbet" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_78">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q5PSB0" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_79" name="GATA3" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_79">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P23772" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_80" name="GATA3-P" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:bqbiol="http://biomodels.net/biology-qualifiers/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_80">
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:pato:PATO%3A0002220" />
      </rdf:Bag>
    </bqbiol:hasProperty>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P23772" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_81" name="STAT6-P" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:bqbiol="http://biomodels.net/biology-qualifiers/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_81">
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:pato:PATO%3A0002220" />
      </rdf:Bag>
    </bqbiol:hasProperty>
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T14:58:19Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P52633" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_82" name="STAT6" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_82">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P52633" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_83" name="PPARg" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_83">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q6GU14" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_84" name="L-PPARg" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_84">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:biomodels.sbo:SBO%3A0000280" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q6GU14" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_85" name="Ligand" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_85">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:biomodels.sbo:SBO%3A0000280" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_86" name="acetylated FOXP3" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:bqbiol="http://biomodels.net/biology-qualifiers/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_86">
    <bqbiol:hasProperty>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:biomodels.sbo:SBO%3A0000215" />
      </rdf:Bag>
    </bqbiol:hasProperty>
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-05T16:43:32Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q99JB6" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_87" name="p19" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_87">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9EQ14" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_88" name="p40" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_88">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P43432" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_89" name="p50" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_89">
    <CopasiMT:isPartOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P25799" />
      </rdf:Bag>
    </CopasiMT:isPartOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_90" name="p65" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_90">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q04207" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_91" name="IL17R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_91">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q60943" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_92" name="IL17-IL17R" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_92">
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:interpro:IPR010345" />
        <rdf:li rdf:resource="urn:miriam:uniprot:Q60943" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_93" name="pIFNg" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_93">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P01580" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_94" name="pIL21" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_94">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:Q9ES17" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_95" name="pIL17" simulationType="fixed" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_95">
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:interpro:IPR010345" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_96" name="IFNAR" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_96">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T12:58:40Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:isVersionOf>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P15261" />
      </rdf:Bag>
    </CopasiMT:isVersionOf>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_97" name="IFNbeta-IFNAR" simulationType="reactions" compartment="Compartment_1" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_97">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T13:01:34Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
    <CopasiMT:hasPart>
      <rdf:Bag>
        <rdf:li rdf:resource="urn:miriam:uniprot:P01580" />
        <rdf:li rdf:resource="urn:miriam:uniprot:P15261" />
      </rdf:Bag>
    </CopasiMT:hasPart>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
    </ListOfMetabolites>
    <ListOfModelValues>
      <ModelValue key="ModelValue_0" name="Hill Coeficient" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_0">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-25T11:26:39Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </ModelValue>
      <ModelValue key="ModelValue_1" name="BActin" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_1">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-12-13T11:42:17Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </ModelValue>
      <ModelValue key="ModelValue_2" name="FOXP3R" simulationType="assignment" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_2">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-12-13T11:40:46Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Expression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[acetylated FOXP3],Reference=Concentration>/&lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[BActin],Reference=InitialValue>
        </Expression>
      </ModelValue>
      <ModelValue key="ModelValue_3" name="IL17R" simulationType="assignment" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_3">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-12-13T11:41:49Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Expression>
          &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL17],Reference=Concentration>/&lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[BActin],Reference=InitialValue>
        </Expression>
      </ModelValue>
      <ModelValue key="ModelValue_4" name="STAT1 coupling" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_4">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T15:31:29Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </ModelValue>
      <ModelValue key="ModelValue_5" name="STAT3_coupling" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_5">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T16:28:30Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </ModelValue>
      <ModelValue key="ModelValue_6" name="STAT5_coupling" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_6">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T17:27:54Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </ModelValue>
      <ModelValue key="ModelValue_7" name="STAT4_coupling" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_7">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T17:43:31Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </ModelValue>
      <ModelValue key="ModelValue_8" name="STAT6_coupling" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_8">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T17:58:31Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </ModelValue>
    </ListOfModelValues>
    <ListOfReactions>
      <Reaction key="Reaction_0" name="re2" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_0">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:07:16Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_10" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_46" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_45" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_81" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_10" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_46" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_45" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5370" name="Vf" value="0.1"/>
          <Constant key="Parameter_5369" name="K" value="0.138094"/>
          <Constant key="Parameter_5368" name="Vr" value="0.1"/>
          <Constant key="Parameter_5367" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_40" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_264">
              <SourceParameter reference="Parameter_5369"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_263">
              <SourceParameter reference="Parameter_5370"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_262">
              <SourceParameter reference="Parameter_5368"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_261">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_250">
              <SourceParameter reference="Metabolite_46"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_265">
              <SourceParameter reference="Metabolite_45"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_266">
              <SourceParameter reference="Metabolite_81"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_267">
              <SourceParameter reference="Metabolite_10"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_1" name="re3" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_1">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:31:54Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_55" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_56" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_45" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_55" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_56" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5366" name="Vf" value="0.1"/>
          <Constant key="Parameter_5365" name="K" value="2.01676"/>
          <Constant key="Parameter_5364" name="Vr" value="0.1"/>
          <Constant key="Parameter_5363" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_41" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_275">
              <SourceParameter reference="Parameter_5365"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_274">
              <SourceParameter reference="Parameter_5366"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_273">
              <SourceParameter reference="Parameter_5364"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_272">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_271">
              <SourceParameter reference="Metabolite_56"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_270">
              <SourceParameter reference="Metabolite_45"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_269">
              <SourceParameter reference="Metabolite_55"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_2" name="re6" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_2">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T10:59:27Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_44" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_44" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5362" name="k1" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="Parameter_5362"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_44"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_3" name="re8" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_3">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:37:46Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_43" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_42" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_81" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_1" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_43" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_42" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5361" name="Vf" value="0.1"/>
          <Constant key="Parameter_5360" name="K1" value="2.94611"/>
          <Constant key="Parameter_5359" name="K2" value="0.743847"/>
          <Constant key="Parameter_5358" name="Vr" value="0.1"/>
          <Constant key="Parameter_5357" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_42" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_281">
              <SourceParameter reference="Parameter_5360"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_280">
              <SourceParameter reference="Parameter_5359"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_279">
              <SourceParameter reference="Parameter_5361"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_278">
              <SourceParameter reference="Parameter_5358"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_277">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_276">
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_268">
              <SourceParameter reference="Metabolite_43"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_282">
              <SourceParameter reference="Metabolite_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_283">
              <SourceParameter reference="Metabolite_81"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_284">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_4" name="re9" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_4">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T09:42:07Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_89" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_90" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_57" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_86" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_56" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_57" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_89" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_90" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5356" name="Vf" value="0.1"/>
          <Constant key="Parameter_5355" name="K1" value="0.1"/>
          <Constant key="Parameter_5354" name="K2" value="98.9482"/>
          <Constant key="Parameter_5353" name="K3" value="0.0539426"/>
          <Constant key="Parameter_5352" name="Vr" value="0.1"/>
          <Constant key="Parameter_5351" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_43" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_294">
              <SourceParameter reference="Parameter_5355"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_293">
              <SourceParameter reference="Parameter_5354"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_292">
              <SourceParameter reference="Parameter_5353"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_291">
              <SourceParameter reference="Parameter_5356"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_290">
              <SourceParameter reference="Parameter_5352"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_289">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_288">
              <SourceParameter reference="Metabolite_56"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_287">
              <SourceParameter reference="Metabolite_57"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_286">
              <SourceParameter reference="Metabolite_86"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_285">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_295">
              <SourceParameter reference="Metabolite_89"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_296">
              <SourceParameter reference="Metabolite_90"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_5" name="STAT4_Phosphorylation" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_5">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T10:58:00Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_58" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_59" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_49" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_80" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_42" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_52" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_97" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5675" name="Vf" value="0.1"/>
          <Constant key="Parameter_5676" name="K1" value="0.125481"/>
          <Constant key="Parameter_5674" name="K2" value="0.896854"/>
          <Constant key="Parameter_5677" name="K3" value="0.031433"/>
          <Constant key="Parameter_5704" name="K4" value="66.6168"/>
          <Constant key="Parameter_5705" name="Vr" value="0.1"/>
          <Constant key="Parameter_5703" name="parameter_1" value="2"/>
          <Constant key="Parameter_5706" name="alpha4" value="0.967"/>
          <Constant key="Parameter_5688" name="K5" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_75" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_606">
              <SourceParameter reference="Parameter_5675"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_605">
              <SourceParameter reference="Metabolite_58"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_604">
              <SourceParameter reference="Parameter_5676"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_603">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_572">
              <SourceParameter reference="Metabolite_49"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_571">
              <SourceParameter reference="Parameter_5674"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_589">
              <SourceParameter reference="Metabolite_80"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_590">
              <SourceParameter reference="Metabolite_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_591">
              <SourceParameter reference="Parameter_5677"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_592">
              <SourceParameter reference="Metabolite_52"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_607">
              <SourceParameter reference="Parameter_5704"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_608">
              <SourceParameter reference="ModelValue_7"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_609">
              <SourceParameter reference="Metabolite_97"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_610">
              <SourceParameter reference="Parameter_5688"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_611">
              <SourceParameter reference="Parameter_5705"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_612">
              <SourceParameter reference="Metabolite_59"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_6" name="re11" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_6">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:22:52Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_41" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_40" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_76" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_0" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_41" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_40" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5689" name="Vf" value="0.1"/>
          <Constant key="Parameter_5687" name="K" value="0.263953"/>
          <Constant key="Parameter_5690" name="Vr" value="0.1"/>
          <Constant key="Parameter_5350" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_44" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_308">
              <SourceParameter reference="Parameter_5687"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_307">
              <SourceParameter reference="Parameter_5689"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_306">
              <SourceParameter reference="Parameter_5690"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_305">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_304">
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_303">
              <SourceParameter reference="Metabolite_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_302">
              <SourceParameter reference="Metabolite_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_301">
              <SourceParameter reference="Metabolite_76"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_7" name="re12" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_7">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:00:50Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_60" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_60" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5349" name="k1" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="Parameter_5349"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_60"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_8" name="STAT1 phosphorylation" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_8">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T10:41:42Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_63" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_64" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_49" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_40" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_62" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_97" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5348" name="Vf" value="0.1"/>
          <Constant key="Parameter_5347" name="K" value="0.1"/>
          <Constant key="Parameter_5346" name="K1" value="1"/>
          <Constant key="Parameter_5345" name="K2" value="0.0705365"/>
          <Constant key="Parameter_5344" name="K3" value="14.9778"/>
          <Constant key="Parameter_5343" name="Vr" value="0.1"/>
          <Constant key="Parameter_5342" name="parameter_1" value="2"/>
          <Constant key="Parameter_5341" name="K4" value="0.1"/>
          <Constant key="Parameter_5340" name="alpha1" value="0.904"/>
        </ListOfConstants>
        <KineticLaw function="Function_73" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_588">
              <SourceParameter reference="Parameter_5348"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_587">
              <SourceParameter reference="Metabolite_63"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_586">
              <SourceParameter reference="Parameter_5347"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_585">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_584">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_583">
              <SourceParameter reference="Metabolite_49"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_582">
              <SourceParameter reference="Parameter_5346"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_581">
              <SourceParameter reference="Metabolite_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_580">
              <SourceParameter reference="Parameter_5345"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_579">
              <SourceParameter reference="Metabolite_62"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_578">
              <SourceParameter reference="Parameter_5344"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_577">
              <SourceParameter reference="Metabolite_97"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_576">
              <SourceParameter reference="Parameter_5341"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_575">
              <SourceParameter reference="Parameter_5343"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_574">
              <SourceParameter reference="Metabolite_64"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_573">
              <SourceParameter reference="ModelValue_4"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_9" name="re14" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_9">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:45:40Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_93" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_60" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_12" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_70" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_59" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_57" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_77" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_93" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_60" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5339" name="Vf" value="0.1"/>
          <Constant key="Parameter_5338" name="K1" value="0.501917"/>
          <Constant key="Parameter_5337" name="K2" value="0.812366"/>
          <Constant key="Parameter_5336" name="K3" value="0.1"/>
          <Constant key="Parameter_5335" name="K4" value="0.001477"/>
          <Constant key="Parameter_5643" name="K5" value="100"/>
          <Constant key="Parameter_5644" name="K6" value="0.230841"/>
          <Constant key="Parameter_5642" name="Vr" value="0.1"/>
          <Constant key="Parameter_5645" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_45" unitType="ConcentrationPerTime" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_312">
              <SourceParameter reference="Parameter_5338"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_311">
              <SourceParameter reference="Parameter_5337"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_310">
              <SourceParameter reference="Parameter_5336"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_309">
              <SourceParameter reference="Parameter_5335"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_297">
              <SourceParameter reference="Parameter_5643"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_298">
              <SourceParameter reference="Parameter_5644"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_299">
              <SourceParameter reference="Parameter_5339"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_300">
              <SourceParameter reference="Parameter_5642"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_313">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_314">
              <SourceParameter reference="Metabolite_57"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_315">
              <SourceParameter reference="Metabolite_59"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_316">
              <SourceParameter reference="Metabolite_77"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_317">
              <SourceParameter reference="Metabolite_70"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_318">
              <SourceParameter reference="Metabolite_60"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_319">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_320">
              <SourceParameter reference="Metabolite_12"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_321">
              <SourceParameter reference="Metabolite_93"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_10" name="re15" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_10">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T11:27:03Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_78" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_77" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_31" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_70" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_59" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_64" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_77" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_78" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5334" name="Vf" value="0.1"/>
          <Constant key="Parameter_5333" name="K2" value="1.33558"/>
          <Constant key="Parameter_5332" name="K3" value="0.001"/>
          <Constant key="Parameter_5331" name="K1" value="0.916783"/>
          <Constant key="Parameter_5330" name="K4" value="0.727962"/>
          <Constant key="Parameter_5329" name="K5" value="6.97805"/>
          <Constant key="Parameter_5328" name="Vr" value="0.1"/>
          <Constant key="Parameter_5324" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_46" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_338">
              <SourceParameter reference="Parameter_5331"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_337">
              <SourceParameter reference="Parameter_5333"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_336">
              <SourceParameter reference="Parameter_5332"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_335">
              <SourceParameter reference="Parameter_5330"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_334">
              <SourceParameter reference="Parameter_5329"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_333">
              <SourceParameter reference="Parameter_5334"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_332">
              <SourceParameter reference="Parameter_5328"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_331">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_330">
              <SourceParameter reference="Metabolite_59"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_329">
              <SourceParameter reference="Metabolite_64"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_328">
              <SourceParameter reference="Metabolite_77"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_327">
              <SourceParameter reference="Metabolite_31"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_326">
              <SourceParameter reference="Metabolite_70"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_325">
              <SourceParameter reference="Metabolite_78"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_11" name="re16" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_11">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:24:16Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_61" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_62" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_76" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_40" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_61" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_62" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5326" name="Vf" value="0.1"/>
          <Constant key="Parameter_5325" name="K1" value="0.1"/>
          <Constant key="Parameter_5323" name="K2" value="0.004433"/>
          <Constant key="Parameter_5327" name="K3" value="99.987"/>
          <Constant key="Parameter_5322" name="Vr" value="0.1"/>
          <Constant key="Parameter_5321" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_47" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_349">
              <SourceParameter reference="Parameter_5325"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_348">
              <SourceParameter reference="Parameter_5323"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_347">
              <SourceParameter reference="Parameter_5327"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_346">
              <SourceParameter reference="Parameter_5326"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_345">
              <SourceParameter reference="Parameter_5322"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_344">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_343">
              <SourceParameter reference="Metabolite_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_342">
              <SourceParameter reference="Metabolite_62"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_341">
              <SourceParameter reference="Metabolite_76"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_340">
              <SourceParameter reference="Metabolite_61"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_339">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_12" name="re17" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_12">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T14:49:48Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_75" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_76" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_64" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_77" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_75" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_76" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5320" name="Vf" value="0.1"/>
          <Constant key="Parameter_5319" name="K1" value="7.83763"/>
          <Constant key="Parameter_5318" name="K2" value="0.667462"/>
          <Constant key="Parameter_5317" name="Vr" value="0.1"/>
          <Constant key="Parameter_5316" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_48" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_357">
              <SourceParameter reference="Parameter_5319"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_356">
              <SourceParameter reference="Parameter_5318"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_355">
              <SourceParameter reference="Parameter_5320"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_354">
              <SourceParameter reference="Parameter_5317"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_353">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_352">
              <SourceParameter reference="Metabolite_64"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_351">
              <SourceParameter reference="Metabolite_77"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_350">
              <SourceParameter reference="Metabolite_76"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_324">
              <SourceParameter reference="Metabolite_75"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_13" name="re18" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_13">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:03:58Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_39" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_4" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_39" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5315" name="k1" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="Parameter_5315"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_39"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_14" name="re19" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_14">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:21:55Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_4" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_38" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_37" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_76" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_4" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_38" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_37" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5314" name="Vf" value="0.1"/>
          <Constant key="Parameter_5308" name="K" value="13.0657"/>
          <Constant key="Parameter_5307" name="Vr" value="0.1"/>
          <Constant key="Parameter_5306" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_49" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_364">
              <SourceParameter reference="Parameter_5308"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_363">
              <SourceParameter reference="Parameter_5314"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_362">
              <SourceParameter reference="Parameter_5307"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_361">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_360">
              <SourceParameter reference="Metabolite_76"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_359">
              <SourceParameter reference="Metabolite_4"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_358">
              <SourceParameter reference="Metabolite_38"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_322">
              <SourceParameter reference="Metabolite_37"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_15" name="re20" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_15">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T14:52:53Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_13" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_39" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_64" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_11" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_39" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_80" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_72" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_13" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5305" name="Vf" value="0.1"/>
          <Constant key="Parameter_5304" name="K1" value="0.210399"/>
          <Constant key="Parameter_5303" name="K2" value="56.3452"/>
          <Constant key="Parameter_5302" name="K3" value="98.0373"/>
          <Constant key="Parameter_5301" name="K4" value="0.855534"/>
          <Constant key="Parameter_5300" name="K5" value="4.32731"/>
          <Constant key="Parameter_5299" name="Vr" value="0.1"/>
          <Constant key="Parameter_5298" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_50" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_371">
              <SourceParameter reference="Parameter_5304"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_370">
              <SourceParameter reference="Parameter_5303"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_369">
              <SourceParameter reference="Parameter_5302"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_368">
              <SourceParameter reference="Parameter_5301"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_367">
              <SourceParameter reference="Parameter_5300"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_366">
              <SourceParameter reference="Parameter_5305"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_365">
              <SourceParameter reference="Parameter_5299"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_323">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_372">
              <SourceParameter reference="Metabolite_64"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_373">
              <SourceParameter reference="Metabolite_39"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_374">
              <SourceParameter reference="Metabolite_80"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_375">
              <SourceParameter reference="Metabolite_72"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_376">
              <SourceParameter reference="Metabolite_11"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_377">
              <SourceParameter reference="Metabolite_13"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_16" name="re23" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_16">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T14:55:16Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_79" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_80" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_77" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_70" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_31" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_81" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_79" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_80" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5297" name="Vf" value="0.1"/>
          <Constant key="Parameter_5296" name="K1" value="0.199351"/>
          <Constant key="Parameter_5295" name="K2" value="9.61521"/>
          <Constant key="Parameter_5294" name="K4" value="1e-05"/>
          <Constant key="Parameter_5293" name="K5" value="1e-05"/>
          <Constant key="Parameter_5292" name="K3" value="0.214012"/>
          <Constant key="Parameter_5291" name="Vr" value="0.1"/>
          <Constant key="Parameter_5290" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_51" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_391">
              <SourceParameter reference="Parameter_5296"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_390">
              <SourceParameter reference="Parameter_5295"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_389">
              <SourceParameter reference="Parameter_5292"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_388">
              <SourceParameter reference="Parameter_5294"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_387">
              <SourceParameter reference="Parameter_5293"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_386">
              <SourceParameter reference="Parameter_5297"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_385">
              <SourceParameter reference="Parameter_5291"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_384">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_383">
              <SourceParameter reference="Metabolite_77"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_382">
              <SourceParameter reference="Metabolite_81"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_381">
              <SourceParameter reference="Metabolite_80"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_380">
              <SourceParameter reference="Metabolite_31"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_379">
              <SourceParameter reference="Metabolite_70"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_378">
              <SourceParameter reference="Metabolite_79"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_392">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_17" name="STAT6 Phosphorylation" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_17">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:27:40Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_82" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_81" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_40" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_37" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_97" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5289" name="Vf" value="0.1"/>
          <Constant key="Parameter_5288" name="K" value="0.1"/>
          <Constant key="Parameter_5287" name="n1" value="0.004304"/>
          <Constant key="Parameter_5286" name="K1" value="0.1"/>
          <Constant key="Parameter_5285" name="Vr" value="0.1"/>
          <Constant key="Parameter_5284" name="parameter_1" value="2"/>
          <Constant key="Parameter_5283" name="alpha6" value="0.249"/>
          <Constant key="Parameter_5282" name="K2" value="0.1"/>
          <Constant key="Parameter_5281" name="paramerter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_76" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_628">
              <SourceParameter reference="Parameter_5289"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_627">
              <SourceParameter reference="Metabolite_82"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_626">
              <SourceParameter reference="Parameter_5288"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_625">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_624">
              <SourceParameter reference="Metabolite_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_623">
              <SourceParameter reference="Metabolite_37"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_622">
              <SourceParameter reference="Parameter_5287"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_621">
              <SourceParameter reference="Parameter_5286"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_620">
              <SourceParameter reference="Parameter_5285"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_619">
              <SourceParameter reference="Metabolite_81"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_618">
              <SourceParameter reference="ModelValue_8"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_617">
              <SourceParameter reference="Metabolite_97"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_616">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_615">
              <SourceParameter reference="Parameter_5282"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_18" name="re25" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_18">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T15:42:51Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_6" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_35" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_34" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_6" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_35" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_34" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5280" name="k1" value="0.1"/>
          <Constant key="Parameter_5279" name="k2" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="Parameter_5280"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_6"/>
              <SourceParameter reference="Metabolite_35"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="Parameter_5279"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_34"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_19" name="STAT5 Phosphorylation" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_19">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:31:32Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_73" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_72" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_34" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_97" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5278" name="Vf" value="0.1"/>
          <Constant key="Parameter_5277" name="K1" value="0.374591"/>
          <Constant key="Parameter_5276" name="Vr" value="0.1"/>
          <Constant key="Parameter_5275" name="parameter_1" value="2"/>
          <Constant key="Parameter_5274" name="K2" value="0.1"/>
          <Constant key="Parameter_5273" name="alpha5" value="0.9897"/>
        </ListOfConstants>
        <KineticLaw function="Function_74" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_602">
              <SourceParameter reference="Parameter_5278"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_601">
              <SourceParameter reference="Metabolite_73"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_600">
              <SourceParameter reference="Metabolite_34"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_599">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_598">
              <SourceParameter reference="Parameter_5277"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_597">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_596">
              <SourceParameter reference="Metabolite_97"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_595">
              <SourceParameter reference="Parameter_5274"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_594">
              <SourceParameter reference="Parameter_5276"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_593">
              <SourceParameter reference="Metabolite_72"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_20" name="re27" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_20">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T15:48:48Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_74" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_86" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_81" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_70" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_28" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_31" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_64" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_72" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_74" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_86" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5272" name="Vf" value="0.1"/>
          <Constant key="Parameter_5271" name="K1" value="100"/>
          <Constant key="Parameter_5270" name="K2" value="0.354892"/>
          <Constant key="Parameter_5269" name="K3" value="1.31281"/>
          <Constant key="Parameter_5268" name="K4" value="0.000679025"/>
          <Constant key="Parameter_5267" name="K5" value="2.07945"/>
          <Constant key="Parameter_5266" name="K6" value="100"/>
          <Constant key="Parameter_5265" name="K7" value="1.93254e-07"/>
          <Constant key="Parameter_5264" name="Vr" value="0.1"/>
          <Constant key="Parameter_5263" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_52" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_407">
              <SourceParameter reference="Parameter_5271"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_406">
              <SourceParameter reference="Parameter_5270"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_405">
              <SourceParameter reference="Parameter_5269"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_404">
              <SourceParameter reference="Parameter_5268"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_403">
              <SourceParameter reference="Parameter_5267"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_402">
              <SourceParameter reference="Parameter_5266"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_401">
              <SourceParameter reference="Parameter_5265"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_400">
              <SourceParameter reference="Parameter_5272"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_399">
              <SourceParameter reference="Parameter_5264"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_398">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_397">
              <SourceParameter reference="Metabolite_64"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_396">
              <SourceParameter reference="Metabolite_81"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_395">
              <SourceParameter reference="Metabolite_72"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_394">
              <SourceParameter reference="Metabolite_86"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_393">
              <SourceParameter reference="Metabolite_31"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_408">
              <SourceParameter reference="Metabolite_28"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_409">
              <SourceParameter reference="Metabolite_70"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_410">
              <SourceParameter reference="Metabolite_74"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_411">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_21" name="re28" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_21">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T16:07:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_5" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_32" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_31" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_5" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_32" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_31" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5262" name="k1" value="0.1"/>
          <Constant key="Parameter_5261" name="k2" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="Parameter_5262"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_5"/>
              <SourceParameter reference="Metabolite_32"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="Parameter_5261"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_31"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_22" name="re29" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_22">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T16:07:33Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_7" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_29" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_28" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_7" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_29" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_28" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5260" name="k1" value="0.1"/>
          <Constant key="Parameter_5259" name="k2" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="Parameter_5260"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_7"/>
              <SourceParameter reference="Metabolite_29"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="Parameter_5259"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_28"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_23" name="STAT3 phosphorylation" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_23">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T16:08:29Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_71" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_70" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_49" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_52" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_54" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_28" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_97" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5258" name="Vf" value="0.1"/>
          <Constant key="Parameter_5257" name="K" value="0.1"/>
          <Constant key="Parameter_5256" name="K1" value="0.639"/>
          <Constant key="Parameter_5255" name="K2" value="39.018"/>
          <Constant key="Parameter_5254" name="K3" value="2.26986"/>
          <Constant key="Parameter_5253" name="K4" value="0.137545"/>
          <Constant key="Parameter_5252" name="Vr" value="0.1"/>
          <Constant key="Parameter_5251" name="parameter_1" value="2"/>
          <Constant key="Parameter_5250" name="K5" value="0.1"/>
          <Constant key="Parameter_5249" name="alpha3" value="0.201"/>
        </ListOfConstants>
        <KineticLaw function="Function_72" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_567">
              <SourceParameter reference="Parameter_5258"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_566">
              <SourceParameter reference="Metabolite_71"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_565">
              <SourceParameter reference="Parameter_5257"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_564">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_563">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_562">
              <SourceParameter reference="Metabolite_49"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_561">
              <SourceParameter reference="Parameter_5256"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_560">
              <SourceParameter reference="Metabolite_52"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_559">
              <SourceParameter reference="Parameter_5255"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_558">
              <SourceParameter reference="Metabolite_54"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_557">
              <SourceParameter reference="Parameter_5254"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_556">
              <SourceParameter reference="Metabolite_28"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_555">
              <SourceParameter reference="Parameter_5253"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_554">
              <SourceParameter reference="Metabolite_97"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_553">
              <SourceParameter reference="Parameter_5252"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_568">
              <SourceParameter reference="Metabolite_70"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_569">
              <SourceParameter reference="Parameter_5250"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_570">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_24" name="re31" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_24">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T16:13:49Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_68" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_69" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_86" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_70" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_31" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_68" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_69" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5248" name="Vf" value="0.2249"/>
          <Constant key="Parameter_5247" name="K1" value="9722.09"/>
          <Constant key="Parameter_5246" name="K2" value="0.703778"/>
          <Constant key="Parameter_5245" name="K3" value="1.24123"/>
          <Constant key="Parameter_5244" name="K4" value="997.263"/>
          <Constant key="Parameter_5243" name="Vr" value="0.1"/>
          <Constant key="Parameter_5242" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_53" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_430">
              <SourceParameter reference="Parameter_5247"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_429">
              <SourceParameter reference="Parameter_5246"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_428">
              <SourceParameter reference="Parameter_5245"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_427">
              <SourceParameter reference="Parameter_5244"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_426">
              <SourceParameter reference="Parameter_5248"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_425">
              <SourceParameter reference="Parameter_5243"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_424">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_423">
              <SourceParameter reference="Metabolite_86"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_422">
              <SourceParameter reference="Metabolite_31"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_421">
              <SourceParameter reference="Metabolite_70"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_420">
              <SourceParameter reference="Metabolite_69"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_419">
              <SourceParameter reference="Metabolite_68"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_418">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_25" name="re32" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_25">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T16:39:27Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_2" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_50" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_49" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_72" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_92" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_2" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_50" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_49" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5241" name="Vf" value="0.1"/>
          <Constant key="Parameter_5240" name="K" value="0.240705"/>
          <Constant key="Parameter_5239" name="K1" value="8.14189"/>
          <Constant key="Parameter_5238" name="Vr" value="0.1"/>
          <Constant key="Parameter_5237" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_54" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_437">
              <SourceParameter reference="Parameter_5240"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_436">
              <SourceParameter reference="Parameter_5239"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_435">
              <SourceParameter reference="Parameter_5241"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_434">
              <SourceParameter reference="Parameter_5238"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_433">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_432">
              <SourceParameter reference="Metabolite_72"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_431">
              <SourceParameter reference="Metabolite_2"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_412">
              <SourceParameter reference="Metabolite_50"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_413">
              <SourceParameter reference="Metabolite_49"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_414">
              <SourceParameter reference="Metabolite_92"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_26" name="re33" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_26">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:00:32Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_48" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_2" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_48" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5236" name="k1" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="Parameter_5236"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_48"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_27" name="re34" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_27">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:03:33Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_3" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_65" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_3" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_65" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5235" name="k1" value="0.1"/>
          <Constant key="Parameter_5234" name="k2" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="Parameter_5235"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_3"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="Parameter_5234"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_65"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_28" name="re35" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_28">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:22:26Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_65" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_51" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_52" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_77" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_65" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_51" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_52" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5233" name="Vf" value="0.1"/>
          <Constant key="Parameter_5232" name="K" value="4.66107"/>
          <Constant key="Parameter_5231" name="Vr" value="0.1"/>
          <Constant key="Parameter_5230" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_55" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_444">
              <SourceParameter reference="Parameter_5232"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_443">
              <SourceParameter reference="Parameter_5233"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_442">
              <SourceParameter reference="Parameter_5231"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_441">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_440">
              <SourceParameter reference="Metabolite_77"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_439">
              <SourceParameter reference="Metabolite_65"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_438">
              <SourceParameter reference="Metabolite_51"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_417">
              <SourceParameter reference="Metabolite_52"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_29" name="re36" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_29">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:04:43Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_88" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_87" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_65" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_28" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_88" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_87" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_65" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5229" name="Vf" value="0.1"/>
          <Constant key="Parameter_5228" name="K" value="0.1"/>
          <Constant key="Parameter_5227" name="K1" value="25.5354"/>
          <Constant key="Parameter_5226" name="Vr" value="0.1"/>
          <Constant key="Parameter_5225" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_56" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_450">
              <SourceParameter reference="Parameter_5228"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_449">
              <SourceParameter reference="Parameter_5227"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_448">
              <SourceParameter reference="Parameter_5229"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_447">
              <SourceParameter reference="Parameter_5226"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_446">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_445">
              <SourceParameter reference="Metabolite_28"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_415">
              <SourceParameter reference="Metabolite_65"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_416">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_451">
              <SourceParameter reference="Metabolite_87"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_452">
              <SourceParameter reference="Metabolite_88"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_30" name="re37" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_30">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T16:38:22Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_94" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_48" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_70" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_94" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_48" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5224" name="Vf" value="0.1"/>
          <Constant key="Parameter_5223" name="K" value="0.118892"/>
          <Constant key="Parameter_5222" name="Vr" value="0.1"/>
          <Constant key="Parameter_5221" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_57" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_462">
              <SourceParameter reference="Parameter_5223"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_461">
              <SourceParameter reference="Parameter_5224"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_460">
              <SourceParameter reference="Parameter_5222"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_459">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_458">
              <SourceParameter reference="Metabolite_70"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_457">
              <SourceParameter reference="Metabolite_48"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_456">
              <SourceParameter reference="Metabolite_94"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_31" name="re38" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_31">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T16:57:38Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_9" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_53" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_54" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_9" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_53" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_54" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5220" name="k1" value="0.1"/>
          <Constant key="Parameter_5219" name="k2" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="Parameter_5220"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_9"/>
              <SourceParameter reference="Metabolite_53"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="Parameter_5219"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_54"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_32" name="re42" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_32">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T17:01:32Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_83" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_85" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_84" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_40" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_37" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_83" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_85" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5218" name="Vf" value="0.1"/>
          <Constant key="Parameter_5217" name="K" value="0.1"/>
          <Constant key="Parameter_5216" name="K1" value="0.1"/>
          <Constant key="Parameter_5215" name="Vr" value="0.1"/>
          <Constant key="Parameter_5214" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_58" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_466">
              <SourceParameter reference="Parameter_5217"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_465">
              <SourceParameter reference="Parameter_5216"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_464">
              <SourceParameter reference="Parameter_5218"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_463">
              <SourceParameter reference="Parameter_5215"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_453">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_454">
              <SourceParameter reference="Metabolite_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_455">
              <SourceParameter reference="Metabolite_37"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_467">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_468">
              <SourceParameter reference="Metabolite_83"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_469">
              <SourceParameter reference="Metabolite_85"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_33" name="re44" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_33">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:04:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_33" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_5" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_33" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5213" name="k1" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="Parameter_5213"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_33"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_34" name="re45" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_34">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:00:17Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_36" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_6" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_36" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5212" name="k1" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="Parameter_5212"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_36"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_35" name="re46" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_35">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:04:02Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_30" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_7" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_30" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5211" name="k1" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="Parameter_5211"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_30"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_36" name="re48" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_36">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-05-05T11:53:11Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_67" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_9" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_67" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5210" name="k1" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="Parameter_5210"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_67"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_37" name="re49" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_37">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-05-05T11:53:12Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_66" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_8" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_66" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5209" name="k1" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="Parameter_5209"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_66"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_38" name="re50" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_38">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-22T17:00:10Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_8" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_91" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_92" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_8" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_91" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_92" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5208" name="k1" value="0.184881"/>
          <Constant key="Parameter_5207" name="k2" value="0.1896"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="Parameter_5208"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_8"/>
              <SourceParameter reference="Metabolite_91"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="Parameter_5207"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_92"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_39" name="re47" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_39">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-05-04T12:34:41Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_47" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_47" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5206" name="k1" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="Parameter_5206"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_47"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_40" name="re51" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_40">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2011-01-21T13:58:19Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_95" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_66" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_52" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_69" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_59" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_95" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_66" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5205" name="Vf" value="0.225095"/>
          <Constant key="Parameter_5860" name="K" value="0.1"/>
          <Constant key="Parameter_5861" name="K1" value="1.62893"/>
          <Constant key="Parameter_5859" name="K2" value="0.526832"/>
          <Constant key="Parameter_5862" name="K3" value="5.47889"/>
          <Constant key="Parameter_5204" name="Vr" value="0.1"/>
          <Constant key="Parameter_5203" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_59" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_479">
              <SourceParameter reference="Parameter_5860"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_478">
              <SourceParameter reference="Parameter_5861"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_477">
              <SourceParameter reference="Parameter_5859"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_476">
              <SourceParameter reference="Parameter_5862"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_475">
              <SourceParameter reference="Parameter_5205"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_474">
              <SourceParameter reference="Parameter_5204"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_473">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_472">
              <SourceParameter reference="Metabolite_59"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_471">
              <SourceParameter reference="Metabolite_69"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_470">
              <SourceParameter reference="Metabolite_52"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_480">
              <SourceParameter reference="Metabolite_66"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_481">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_482">
              <SourceParameter reference="Metabolite_95"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_41" name="IL18 pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_41">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-01-17T10:13:46Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_14" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5202" name="V" value="0.1"/>
          <Constant key="Parameter_5201" name="k" value="0.5"/>
          <Constant key="Parameter_5200" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_60" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_495">
              <SourceParameter reference="Parameter_5202"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_494">
              <SourceParameter reference="Parameter_5201"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_493">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_492">
              <SourceParameter reference="Metabolite_10"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_491">
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_42" name="IL12 pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_42">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-01-17T10:15:57Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_15" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_15" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5199" name="V" value="0.1"/>
          <Constant key="Parameter_5198" name="k" value="0.5"/>
          <Constant key="Parameter_5197" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_61" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_486">
              <SourceParameter reference="Parameter_5199"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_487">
              <SourceParameter reference="Parameter_5198"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_488">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_489">
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_490">
              <SourceParameter reference="Metabolite_15"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_43" name="IFNg pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_43">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-01-17T10:18:51Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_16" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_16" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5193" name="V" value="0.1"/>
          <Constant key="Parameter_5194" name="k" value="0.1"/>
          <Constant key="Parameter_5196" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_62" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_497">
              <SourceParameter reference="Parameter_5193"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_496">
              <SourceParameter reference="Parameter_5194"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_483">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_484">
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_485">
              <SourceParameter reference="Metabolite_16"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_44" name="IL21 pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_44">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-01-17T10:21:26Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_17" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_2" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_17" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_2" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5195" name="V" value="0.1"/>
          <Constant key="Parameter_5192" name="k" value="0.1"/>
          <Constant key="Parameter_5191" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_63" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_502">
              <SourceParameter reference="Parameter_5195"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_501">
              <SourceParameter reference="Parameter_5192"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_500">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_499">
              <SourceParameter reference="Metabolite_2"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_498">
              <SourceParameter reference="Metabolite_17"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_45" name="IL23 pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_45">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-01-17T10:21:24Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_18" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_3" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_18" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_3" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5190" name="V" value="0.1"/>
          <Constant key="Parameter_5189" name="k" value="0.1"/>
          <Constant key="Parameter_5188" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_64" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_507">
              <SourceParameter reference="Parameter_5190"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_506">
              <SourceParameter reference="Parameter_5189"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_505">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_504">
              <SourceParameter reference="Metabolite_3"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_503">
              <SourceParameter reference="Metabolite_18"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_46" name="IL17 pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_46">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-01-17T10:34:45Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_19" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_66" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_19" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_66" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5187" name="V" value="0.1"/>
          <Constant key="Parameter_5186" name="k" value="0.1"/>
          <Constant key="Parameter_5185" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_65" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_512">
              <SourceParameter reference="Parameter_5187"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_511">
              <SourceParameter reference="Parameter_5186"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_510">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_509">
              <SourceParameter reference="Metabolite_66"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_508">
              <SourceParameter reference="Metabolite_19"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_47" name="IL10 pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_47">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-01-17T10:36:48Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_20" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_9" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_20" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_9" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5184" name="V" value="0.1"/>
          <Constant key="Parameter_5183" name="k" value="0.1"/>
          <Constant key="Parameter_5182" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_66" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_517">
              <SourceParameter reference="Parameter_5184"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_516">
              <SourceParameter reference="Parameter_5183"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_515">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_514">
              <SourceParameter reference="Metabolite_9"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_513">
              <SourceParameter reference="Metabolite_20"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_48" name="IL6 pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_48">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-01-17T10:39:52Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_21" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_7" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_21" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_7" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5181" name="V" value="0.1"/>
          <Constant key="Parameter_5180" name="k" value="0.1"/>
          <Constant key="Parameter_5179" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_67" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_522">
              <SourceParameter reference="Parameter_5181"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_521">
              <SourceParameter reference="Parameter_5180"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_520">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_519">
              <SourceParameter reference="Metabolite_7"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_518">
              <SourceParameter reference="Metabolite_21"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_49" name="IL2 pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_49">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-01-17T10:41:25Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_22" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_6" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_22" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_6" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5178" name="V" value="0.1"/>
          <Constant key="Parameter_5177" name="k" value="0.1"/>
          <Constant key="Parameter_5176" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_68" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_527">
              <SourceParameter reference="Parameter_5178"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_526">
              <SourceParameter reference="Parameter_5177"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_525">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_524">
              <SourceParameter reference="Metabolite_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_523">
              <SourceParameter reference="Metabolite_22"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_50" name="TGFb pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_50">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-01-17T10:42:31Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_23" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_5" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_23" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_5" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5175" name="V" value="0.1"/>
          <Constant key="Parameter_5174" name="k" value="0.1"/>
          <Constant key="Parameter_5173" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_69" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_532">
              <SourceParameter reference="Parameter_5175"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_531">
              <SourceParameter reference="Parameter_5174"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_530">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_529">
              <SourceParameter reference="Metabolite_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_528">
              <SourceParameter reference="Metabolite_23"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_51" name="IL4 pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_51">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-01-17T10:43:50Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_24" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_4" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_24" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_4" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5172" name="V" value="0.1"/>
          <Constant key="Parameter_5171" name="k" value="0.1"/>
          <Constant key="Parameter_5170" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_70" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_537">
              <SourceParameter reference="Parameter_5172"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_536">
              <SourceParameter reference="Parameter_5171"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_535">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_534">
              <SourceParameter reference="Metabolite_4"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_533">
              <SourceParameter reference="Metabolite_24"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_52" name="re52" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_52">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2012-02-18T11:35:22Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_25" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_67" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_52" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_84" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_49" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_80" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_72" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_25" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_67" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5169" name="Vf" value="0.1"/>
          <Constant key="Parameter_5168" name="K" value="0.508159"/>
          <Constant key="Parameter_5167" name="K1" value="0.1"/>
          <Constant key="Parameter_5166" name="K2" value="0.00125893"/>
          <Constant key="Parameter_5165" name="K3" value="0.645162"/>
          <Constant key="Parameter_5164" name="K4" value="100"/>
          <Constant key="Parameter_5163" name="Vr" value="0.1"/>
          <Constant key="Parameter_5162" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_71" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_542">
              <SourceParameter reference="Parameter_5168"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_541">
              <SourceParameter reference="Parameter_5167"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_540">
              <SourceParameter reference="Parameter_5166"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_539">
              <SourceParameter reference="Parameter_5165"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_538">
              <SourceParameter reference="Parameter_5164"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_543">
              <SourceParameter reference="Parameter_5169"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_544">
              <SourceParameter reference="Parameter_5163"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_545">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_546">
              <SourceParameter reference="Metabolite_80"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_547">
              <SourceParameter reference="Metabolite_72"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_548">
              <SourceParameter reference="Metabolite_49"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_549">
              <SourceParameter reference="Metabolite_52"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_550">
              <SourceParameter reference="Metabolite_67"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_551">
              <SourceParameter reference="Metabolite_84"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_552">
              <SourceParameter reference="Metabolite_25"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_53" name="IFNbeta binding to receptor" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_53">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2010-02-19T11:22:52Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_26" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_96" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_97" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfModifiers>
          <Modifier metabolite="Metabolite_76" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_26" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_96" stoichiometry="1"/>
          <Modifier metabolite="Metabolite_97" stoichiometry="1"/>
        </ListOfModifiers>
        <ListOfConstants>
          <Constant key="Parameter_5161" name="K" value="0.263953"/>
          <Constant key="Parameter_5160" name="Vf" value="0.1"/>
          <Constant key="Parameter_5159" name="Vr" value="0.1"/>
          <Constant key="Parameter_5158" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_44" unitType="Default">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_308">
              <SourceParameter reference="Parameter_5161"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_307">
              <SourceParameter reference="Parameter_5160"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_306">
              <SourceParameter reference="Parameter_5159"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_305">
              <SourceParameter reference="Parameter_5158"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_304">
              <SourceParameter reference="Metabolite_26"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_303">
              <SourceParameter reference="Metabolite_96"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_302">
              <SourceParameter reference="Metabolite_97"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_301">
              <SourceParameter reference="Metabolite_76"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_54" name="IFNbeta pool" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_54">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2017-01-08T14:52:55Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_27" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_26" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5157" name="V" value="0.1"/>
          <Constant key="Parameter_5156" name="k" value="0.1"/>
          <Constant key="Parameter_5155" name="parameter_1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_62" unitType="Default" scalingCompartment="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_497">
              <SourceParameter reference="Parameter_5157"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_496">
              <SourceParameter reference="Parameter_5156"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_483">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_484">
              <SourceParameter reference="Metabolite_26"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_485">
              <SourceParameter reference="Metabolite_27"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
    </ListOfReactions>
    <ListOfModelParameterSets activeSet="ModelParameterSet_1">
      <ModelParameterSet key="ModelParameterSet_1" name="Initial State">
        <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelParameterSet_1">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-12-14T21:54:16Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ModelParameterGroup cn="String=Initial Time" type="Group">
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization" value="0" type="Model" simulationType="time"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Compartment Sizes" type="Group">
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment]" value="1" type="Compartment" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells]" value="1" type="Compartment" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Species Values" type="Group">
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIFNg]" value="6.0221417899999999e+23" type="Species" simulationType="reactions">
            <InitialExpression>
              &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IFNg_pool],Reference=InitialConcentration>
            </InitialExpression>
          </ModelParameter>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL12]" value="6.0221417899999999e+23" type="Species" simulationType="reactions">
            <InitialExpression>
              &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL12_pool],Reference=InitialConcentration>
            </InitialExpression>
          </ModelParameter>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL21]" value="0" type="Species" simulationType="reactions">
            <InitialExpression>
              &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL21_pool],Reference=InitialConcentration>
            </InitialExpression>
          </ModelParameter>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL23]" value="0" type="Species" simulationType="reactions">
            <InitialExpression>
              &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL23_pool],Reference=InitialConcentration>
            </InitialExpression>
          </ModelParameter>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL4]" value="0" type="Species" simulationType="reactions">
            <InitialExpression>
              &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL4_pool],Reference=InitialConcentration>
            </InitialExpression>
          </ModelParameter>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eTGFb]" value="0" type="Species" simulationType="reactions">
            <InitialExpression>
              &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[TGFb_pool],Reference=InitialConcentration>
            </InitialExpression>
          </ModelParameter>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL2]" value="0" type="Species" simulationType="reactions">
            <InitialExpression>
              &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL2_pool],Reference=InitialConcentration>
            </InitialExpression>
          </ModelParameter>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL6]" value="0" type="Species" simulationType="reactions">
            <InitialExpression>
              &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL6_pool],Reference=InitialConcentration>
            </InitialExpression>
          </ModelParameter>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL17]" value="0" type="Species" simulationType="reactions">
            <InitialExpression>
              &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL17_pool],Reference=InitialConcentration>
            </InitialExpression>
          </ModelParameter>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL10]" value="0" type="Species" simulationType="reactions">
            <InitialExpression>
              &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL10_pool],Reference=InitialConcentration>
            </InitialExpression>
          </ModelParameter>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL18]" value="6.0221417899999999e+23" type="Species" simulationType="reactions">
            <InitialExpression>
              &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL18_pool],Reference=InitialConcentration>
            </InitialExpression>
          </ModelParameter>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[anti-IL4]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[anti-IFNg]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[pIL4]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL18_pool]" value="6.0221417899999999e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL12_pool]" value="6.0221417899999999e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IFNg_pool]" value="6.0221417899999999e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL21_pool]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL23_pool]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL17_pool]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL10_pool]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL6_pool]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL2_pool]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[TGFb_pool]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IL4_pool]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[pIL10]" value="6.0221417899999999e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIFNbeta]" value="2.38476814884e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[IFNbeta_pool]" value="0" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL6-IL6R]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL6R]" value="6.0221414997327648e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[TGFb-TGFbR]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[TGFbR]" value="6.0221414999999983e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[TGFb]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL2-IL2R]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL2R]" value="6.0221414999999983e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL4-IL4R]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL4R]" value="6.0221414999999983e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IFNg-IFNgR]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IFNgR]" value="6.0221414999999983e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL12-IL12R]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL12R]" value="6.0221414999999983e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL12]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL18-IL18R]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL18R]" value="6.0221414999999983e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL18]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL21]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL21-IL21R]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL21R]" value="6.0221414999999983e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL23R]" value="6.0221414999999983e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL23-IL23R]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL10R]" value="6.0221414999999983e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL10-IL10R]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IRAK1]" value="3.0110708949999999e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IRAK1-P]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[p50/p65 dimer]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT4]" value="6.0221414999999983e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT4-P]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IFNg]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[JAK1]" value="3.0110708949999999e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[JAK1-P]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT1]" value="6.0221414999999983e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT1-P]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[p40/p19 dimer]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL17]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL10]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[RORgt]" value="6.0221414999999983e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[RORgt-ligand]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT3-P]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT3]" value="6.0221414999999983e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT5-P]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT5]" value="6.0221414997327648e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[FOXP3]" value="6.0221414999999983e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[SOCS1]" value="6.0221414999999983e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[SOCS1-JAKs]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[Tbet-P]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[Tbet]" value="6.0221414999999983e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[GATA3]" value="6.0221414999999983e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[GATA3-P]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT6-P]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT6]" value="6.0221417899999999e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[PPARg]" value="4.8177134320000002e+22" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[L-PPARg]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[Ligand]" value="6.0221414997327648e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[acetylated FOXP3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[p19]" value="6.0221414997327648e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[p40]" value="6.0221414997327648e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[p50]" value="3.0110708949999999e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[p65]" value="3.0110708949999999e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL17R]" value="6.0221414997327648e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL17-IL17R]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[pIFNg]" value="6.0221414997327648e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[pIL21]" value="6.0221414997327648e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[pIL17]" value="6.0221417899999999e+23" type="Species" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IFNAR]" value="6.0221414997327648e+23" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IFNbeta-IFNAR]" value="0" type="Species" simulationType="reactions"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Global Quantities" type="Group">
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[BActin]" value="0.0085060778101233095" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[FOXP3R]" value="0" type="ModelValue" simulationType="assignment"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[IL17R]" value="0" type="ModelValue" simulationType="assignment"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT1 coupling]" value="0.90400000000000003" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT3_coupling]" value="0.20100000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT5_coupling]" value="0.98970000000000002" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT4_coupling]" value="0.96699999999999997" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT6_coupling]" value="0.249" type="ModelValue" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Kinetic Parameters" type="Group">
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re2],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re2],ParameterGroup=Parameters,Parameter=K" value="0.13809399999999999" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re2],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re2],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re3],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re3],ParameterGroup=Parameters,Parameter=K" value="2.0167600000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re3],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re3],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re6],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re8]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re8],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re8],ParameterGroup=Parameters,Parameter=K1" value="2.94611" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re8],ParameterGroup=Parameters,Parameter=K2" value="0.74384700000000004" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re8],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re8],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re9]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re9],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re9],ParameterGroup=Parameters,Parameter=K1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re9],ParameterGroup=Parameters,Parameter=K2" value="98.9482" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re9],ParameterGroup=Parameters,Parameter=K3" value="0.0539426" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re9],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re9],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT4_Phosphorylation]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT4_Phosphorylation],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT4_Phosphorylation],ParameterGroup=Parameters,Parameter=K1" value="0.12548100000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT4_Phosphorylation],ParameterGroup=Parameters,Parameter=K2" value="0.89685400000000004" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT4_Phosphorylation],ParameterGroup=Parameters,Parameter=K3" value="0.031433000000000003" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT4_Phosphorylation],ParameterGroup=Parameters,Parameter=K4" value="66.616799999999998" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT4_Phosphorylation],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT4_Phosphorylation],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT4_Phosphorylation],ParameterGroup=Parameters,Parameter=alpha4" value="0.96699999999999997" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT4_coupling],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT4_Phosphorylation],ParameterGroup=Parameters,Parameter=K5" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re11]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re11],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re11],ParameterGroup=Parameters,Parameter=K" value="0.26395299999999999" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re11],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re11],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re12]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re12],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT1 phosphorylation]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT1 phosphorylation],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT1 phosphorylation],ParameterGroup=Parameters,Parameter=K" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT1 phosphorylation],ParameterGroup=Parameters,Parameter=K1" value="1" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT1 phosphorylation],ParameterGroup=Parameters,Parameter=K2" value="0.070536500000000002" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT1 phosphorylation],ParameterGroup=Parameters,Parameter=K3" value="14.9778" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT1 phosphorylation],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT1 phosphorylation],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT1 phosphorylation],ParameterGroup=Parameters,Parameter=K4" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT1 phosphorylation],ParameterGroup=Parameters,Parameter=alpha1" value="0.90400000000000003" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT1 coupling],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re14]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re14],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re14],ParameterGroup=Parameters,Parameter=K1" value="0.50191699999999995" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re14],ParameterGroup=Parameters,Parameter=K2" value="0.81236600000000003" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re14],ParameterGroup=Parameters,Parameter=K3" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re14],ParameterGroup=Parameters,Parameter=K4" value="0.001477" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re14],ParameterGroup=Parameters,Parameter=K5" value="100" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re14],ParameterGroup=Parameters,Parameter=K6" value="0.23084099999999999" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re14],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re14],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re15]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re15],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re15],ParameterGroup=Parameters,Parameter=K2" value="1.33558" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re15],ParameterGroup=Parameters,Parameter=K3" value="0.001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re15],ParameterGroup=Parameters,Parameter=K1" value="0.91678300000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re15],ParameterGroup=Parameters,Parameter=K4" value="0.727962" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re15],ParameterGroup=Parameters,Parameter=K5" value="6.9780499999999996" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re15],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re15],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re16]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re16],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re16],ParameterGroup=Parameters,Parameter=K1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re16],ParameterGroup=Parameters,Parameter=K2" value="0.0044330000000000003" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re16],ParameterGroup=Parameters,Parameter=K3" value="99.986999999999995" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re16],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re16],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re17]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re17],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re17],ParameterGroup=Parameters,Parameter=K1" value="7.8376299999999999" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re17],ParameterGroup=Parameters,Parameter=K2" value="0.667462" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re17],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re17],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re18]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re18],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re19]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re19],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re19],ParameterGroup=Parameters,Parameter=K" value="13.0657" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re19],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re19],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re20]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re20],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re20],ParameterGroup=Parameters,Parameter=K1" value="0.210399" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re20],ParameterGroup=Parameters,Parameter=K2" value="56.345199999999998" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re20],ParameterGroup=Parameters,Parameter=K3" value="98.037300000000002" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re20],ParameterGroup=Parameters,Parameter=K4" value="0.85553400000000002" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re20],ParameterGroup=Parameters,Parameter=K5" value="4.3273099999999998" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re20],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re20],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re23]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re23],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re23],ParameterGroup=Parameters,Parameter=K1" value="0.199351" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re23],ParameterGroup=Parameters,Parameter=K2" value="9.6152099999999994" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re23],ParameterGroup=Parameters,Parameter=K4" value="1.0000000000000001e-05" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re23],ParameterGroup=Parameters,Parameter=K5" value="1.0000000000000001e-05" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re23],ParameterGroup=Parameters,Parameter=K3" value="0.21401200000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re23],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re23],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT6 Phosphorylation]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT6 Phosphorylation],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT6 Phosphorylation],ParameterGroup=Parameters,Parameter=K" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT6 Phosphorylation],ParameterGroup=Parameters,Parameter=n1" value="0.0043039999999999997" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT6 Phosphorylation],ParameterGroup=Parameters,Parameter=K1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT6 Phosphorylation],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT6 Phosphorylation],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT6 Phosphorylation],ParameterGroup=Parameters,Parameter=alpha6" value="0.249" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT6_coupling],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT6 Phosphorylation],ParameterGroup=Parameters,Parameter=K2" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT6 Phosphorylation],ParameterGroup=Parameters,Parameter=paramerter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re25]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re25],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re25],ParameterGroup=Parameters,Parameter=k2" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT5 Phosphorylation]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT5 Phosphorylation],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT5 Phosphorylation],ParameterGroup=Parameters,Parameter=K1" value="0.37459100000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT5 Phosphorylation],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT5 Phosphorylation],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT5 Phosphorylation],ParameterGroup=Parameters,Parameter=K2" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT5 Phosphorylation],ParameterGroup=Parameters,Parameter=alpha5" value="0.98970000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT5_coupling],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re27]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re27],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re27],ParameterGroup=Parameters,Parameter=K1" value="100" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re27],ParameterGroup=Parameters,Parameter=K2" value="0.35489199999999999" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re27],ParameterGroup=Parameters,Parameter=K3" value="1.31281" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re27],ParameterGroup=Parameters,Parameter=K4" value="0.00067902499999999998" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re27],ParameterGroup=Parameters,Parameter=K5" value="2.07945" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re27],ParameterGroup=Parameters,Parameter=K6" value="100" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re27],ParameterGroup=Parameters,Parameter=K7" value="1.9325399999999999e-07" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re27],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re27],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re28]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re28],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re28],ParameterGroup=Parameters,Parameter=k2" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re29]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re29],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re29],ParameterGroup=Parameters,Parameter=k2" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT3 phosphorylation]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT3 phosphorylation],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT3 phosphorylation],ParameterGroup=Parameters,Parameter=K" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT3 phosphorylation],ParameterGroup=Parameters,Parameter=K1" value="0.63900000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT3 phosphorylation],ParameterGroup=Parameters,Parameter=K2" value="39.018000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT3 phosphorylation],ParameterGroup=Parameters,Parameter=K3" value="2.26986" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT3 phosphorylation],ParameterGroup=Parameters,Parameter=K4" value="0.137545" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT3 phosphorylation],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT3 phosphorylation],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT3 phosphorylation],ParameterGroup=Parameters,Parameter=K5" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[STAT3 phosphorylation],ParameterGroup=Parameters,Parameter=alpha3" value="0.20100000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT3_coupling],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re31]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re31],ParameterGroup=Parameters,Parameter=Vf" value="0.22489999999999999" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re31],ParameterGroup=Parameters,Parameter=K1" value="9722.0900000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re31],ParameterGroup=Parameters,Parameter=K2" value="0.70377800000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re31],ParameterGroup=Parameters,Parameter=K3" value="1.2412300000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re31],ParameterGroup=Parameters,Parameter=K4" value="997.26300000000003" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re31],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re31],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re32]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re32],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re32],ParameterGroup=Parameters,Parameter=K" value="0.240705" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re32],ParameterGroup=Parameters,Parameter=K1" value="8.1418900000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re32],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re32],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re33]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re33],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re34]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re34],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re34],ParameterGroup=Parameters,Parameter=k2" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re35]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re35],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re35],ParameterGroup=Parameters,Parameter=K" value="4.6610699999999996" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re35],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re35],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re36]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re36],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re36],ParameterGroup=Parameters,Parameter=K" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re36],ParameterGroup=Parameters,Parameter=K1" value="25.535399999999999" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re36],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re36],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re37]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re37],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re37],ParameterGroup=Parameters,Parameter=K" value="0.118892" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re37],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re37],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re38]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re38],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re38],ParameterGroup=Parameters,Parameter=k2" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re42]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re42],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re42],ParameterGroup=Parameters,Parameter=K" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re42],ParameterGroup=Parameters,Parameter=K1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re42],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re42],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re44]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re44],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re45]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re45],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re46]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re46],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re48]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re48],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re49]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re49],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re50]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re50],ParameterGroup=Parameters,Parameter=k1" value="0.18488099999999999" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re50],ParameterGroup=Parameters,Parameter=k2" value="0.18959999999999999" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re47]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re47],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re51]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re51],ParameterGroup=Parameters,Parameter=Vf" value="0.22509499999999999" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re51],ParameterGroup=Parameters,Parameter=K" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re51],ParameterGroup=Parameters,Parameter=K1" value="1.62893" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re51],ParameterGroup=Parameters,Parameter=K2" value="0.52683199999999997" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re51],ParameterGroup=Parameters,Parameter=K3" value="5.4788899999999998" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re51],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re51],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL18 pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL18 pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL18 pool],ParameterGroup=Parameters,Parameter=k" value="0.5" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL18 pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL12 pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL12 pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL12 pool],ParameterGroup=Parameters,Parameter=k" value="0.5" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL12 pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNg pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNg pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNg pool],ParameterGroup=Parameters,Parameter=k" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNg pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL21 pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL21 pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL21 pool],ParameterGroup=Parameters,Parameter=k" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL21 pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL23 pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL23 pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL23 pool],ParameterGroup=Parameters,Parameter=k" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL23 pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL17 pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL17 pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL17 pool],ParameterGroup=Parameters,Parameter=k" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL17 pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL10 pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL10 pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL10 pool],ParameterGroup=Parameters,Parameter=k" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL10 pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL6 pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL6 pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL6 pool],ParameterGroup=Parameters,Parameter=k" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL6 pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL2 pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL2 pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL2 pool],ParameterGroup=Parameters,Parameter=k" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL2 pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[TGFb pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[TGFb pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[TGFb pool],ParameterGroup=Parameters,Parameter=k" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[TGFb pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL4 pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL4 pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL4 pool],ParameterGroup=Parameters,Parameter=k" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IL4 pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re52]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re52],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re52],ParameterGroup=Parameters,Parameter=K" value="0.50815900000000003" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re52],ParameterGroup=Parameters,Parameter=K1" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re52],ParameterGroup=Parameters,Parameter=K2" value="0.0012589299999999999" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re52],ParameterGroup=Parameters,Parameter=K3" value="0.64516200000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re52],ParameterGroup=Parameters,Parameter=K4" value="100" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re52],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[re52],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNbeta binding to receptor]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNbeta binding to receptor],ParameterGroup=Parameters,Parameter=K" value="0.26395299999999999" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNbeta binding to receptor],ParameterGroup=Parameters,Parameter=Vf" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNbeta binding to receptor],ParameterGroup=Parameters,Parameter=Vr" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNbeta binding to receptor],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="fixed"/>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNbeta pool]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNbeta pool],ParameterGroup=Parameters,Parameter=V" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNbeta pool],ParameterGroup=Parameters,Parameter=k" value="0.10000000000000001" type="ReactionParameter" simulationType="fixed"/>
            <ModelParameter cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Reactions[IFNbeta pool],ParameterGroup=Parameters,Parameter=parameter_1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[Hill Coeficient],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
        </ModelParameterGroup>
      </ModelParameterSet>
    </ListOfModelParameterSets>
    <StateTemplate>
      <StateTemplateVariable objectReference="Model_1"/>
      <StateTemplateVariable objectReference="Metabolite_0"/>
      <StateTemplateVariable objectReference="Metabolite_1"/>
      <StateTemplateVariable objectReference="Metabolite_2"/>
      <StateTemplateVariable objectReference="Metabolite_4"/>
      <StateTemplateVariable objectReference="Metabolite_5"/>
      <StateTemplateVariable objectReference="Metabolite_6"/>
      <StateTemplateVariable objectReference="Metabolite_7"/>
      <StateTemplateVariable objectReference="Metabolite_9"/>
      <StateTemplateVariable objectReference="Metabolite_10"/>
      <StateTemplateVariable objectReference="Metabolite_65"/>
      <StateTemplateVariable objectReference="Metabolite_66"/>
      <StateTemplateVariable objectReference="Metabolite_39"/>
      <StateTemplateVariable objectReference="Metabolite_48"/>
      <StateTemplateVariable objectReference="Metabolite_60"/>
      <StateTemplateVariable objectReference="Metabolite_3"/>
      <StateTemplateVariable objectReference="Metabolite_8"/>
      <StateTemplateVariable objectReference="Metabolite_67"/>
      <StateTemplateVariable objectReference="Metabolite_56"/>
      <StateTemplateVariable objectReference="Metabolite_57"/>
      <StateTemplateVariable objectReference="Metabolite_59"/>
      <StateTemplateVariable objectReference="Metabolite_62"/>
      <StateTemplateVariable objectReference="Metabolite_64"/>
      <StateTemplateVariable objectReference="Metabolite_69"/>
      <StateTemplateVariable objectReference="Metabolite_70"/>
      <StateTemplateVariable objectReference="Metabolite_72"/>
      <StateTemplateVariable objectReference="Metabolite_76"/>
      <StateTemplateVariable objectReference="Metabolite_77"/>
      <StateTemplateVariable objectReference="Metabolite_80"/>
      <StateTemplateVariable objectReference="Metabolite_81"/>
      <StateTemplateVariable objectReference="Metabolite_83"/>
      <StateTemplateVariable objectReference="Metabolite_86"/>
      <StateTemplateVariable objectReference="Metabolite_96"/>
      <StateTemplateVariable objectReference="Metabolite_47"/>
      <StateTemplateVariable objectReference="Metabolite_34"/>
      <StateTemplateVariable objectReference="Metabolite_30"/>
      <StateTemplateVariable objectReference="Metabolite_31"/>
      <StateTemplateVariable objectReference="Metabolite_42"/>
      <StateTemplateVariable objectReference="Metabolite_54"/>
      <StateTemplateVariable objectReference="Metabolite_50"/>
      <StateTemplateVariable objectReference="Metabolite_51"/>
      <StateTemplateVariable objectReference="Metabolite_37"/>
      <StateTemplateVariable objectReference="Metabolite_40"/>
      <StateTemplateVariable objectReference="Metabolite_36"/>
      <StateTemplateVariable objectReference="Metabolite_33"/>
      <StateTemplateVariable objectReference="Metabolite_28"/>
      <StateTemplateVariable objectReference="Metabolite_44"/>
      <StateTemplateVariable objectReference="Metabolite_45"/>
      <StateTemplateVariable objectReference="Metabolite_91"/>
      <StateTemplateVariable objectReference="Metabolite_46"/>
      <StateTemplateVariable objectReference="Metabolite_92"/>
      <StateTemplateVariable objectReference="Metabolite_38"/>
      <StateTemplateVariable objectReference="Metabolite_53"/>
      <StateTemplateVariable objectReference="Metabolite_43"/>
      <StateTemplateVariable objectReference="Metabolite_41"/>
      <StateTemplateVariable objectReference="Metabolite_35"/>
      <StateTemplateVariable objectReference="Metabolite_84"/>
      <StateTemplateVariable objectReference="Metabolite_49"/>
      <StateTemplateVariable objectReference="Metabolite_52"/>
      <StateTemplateVariable objectReference="Metabolite_32"/>
      <StateTemplateVariable objectReference="Metabolite_29"/>
      <StateTemplateVariable objectReference="Metabolite_97"/>
      <StateTemplateVariable objectReference="ModelValue_2"/>
      <StateTemplateVariable objectReference="ModelValue_3"/>
      <StateTemplateVariable objectReference="Metabolite_11"/>
      <StateTemplateVariable objectReference="Metabolite_12"/>
      <StateTemplateVariable objectReference="Metabolite_13"/>
      <StateTemplateVariable objectReference="Metabolite_14"/>
      <StateTemplateVariable objectReference="Metabolite_15"/>
      <StateTemplateVariable objectReference="Metabolite_16"/>
      <StateTemplateVariable objectReference="Metabolite_17"/>
      <StateTemplateVariable objectReference="Metabolite_18"/>
      <StateTemplateVariable objectReference="Metabolite_19"/>
      <StateTemplateVariable objectReference="Metabolite_20"/>
      <StateTemplateVariable objectReference="Metabolite_21"/>
      <StateTemplateVariable objectReference="Metabolite_22"/>
      <StateTemplateVariable objectReference="Metabolite_23"/>
      <StateTemplateVariable objectReference="Metabolite_24"/>
      <StateTemplateVariable objectReference="Metabolite_25"/>
      <StateTemplateVariable objectReference="Metabolite_26"/>
      <StateTemplateVariable objectReference="Metabolite_27"/>
      <StateTemplateVariable objectReference="Metabolite_55"/>
      <StateTemplateVariable objectReference="Metabolite_58"/>
      <StateTemplateVariable objectReference="Metabolite_61"/>
      <StateTemplateVariable objectReference="Metabolite_63"/>
      <StateTemplateVariable objectReference="Metabolite_68"/>
      <StateTemplateVariable objectReference="Metabolite_71"/>
      <StateTemplateVariable objectReference="Metabolite_73"/>
      <StateTemplateVariable objectReference="Metabolite_74"/>
      <StateTemplateVariable objectReference="Metabolite_75"/>
      <StateTemplateVariable objectReference="Metabolite_78"/>
      <StateTemplateVariable objectReference="Metabolite_79"/>
      <StateTemplateVariable objectReference="Metabolite_82"/>
      <StateTemplateVariable objectReference="Metabolite_85"/>
      <StateTemplateVariable objectReference="Metabolite_87"/>
      <StateTemplateVariable objectReference="Metabolite_88"/>
      <StateTemplateVariable objectReference="Metabolite_89"/>
      <StateTemplateVariable objectReference="Metabolite_90"/>
      <StateTemplateVariable objectReference="Metabolite_93"/>
      <StateTemplateVariable objectReference="Metabolite_94"/>
      <StateTemplateVariable objectReference="Metabolite_95"/>
      <StateTemplateVariable objectReference="Compartment_0"/>
      <StateTemplateVariable objectReference="Compartment_1"/>
      <StateTemplateVariable objectReference="ModelValue_0"/>
      <StateTemplateVariable objectReference="ModelValue_1"/>
      <StateTemplateVariable objectReference="ModelValue_4"/>
      <StateTemplateVariable objectReference="ModelValue_5"/>
      <StateTemplateVariable objectReference="ModelValue_6"/>
      <StateTemplateVariable objectReference="ModelValue_7"/>
      <StateTemplateVariable objectReference="ModelValue_8"/>
    </StateTemplate>
    <InitialState type="initialState">
      0 6.0221417899999999e+23 6.0221417899999999e+23 0 0 0 0 0 0 6.0221417899999999e+23 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4.8177134320000002e+22 0 6.0221414997327648e+23 0 0 0 0 0 0 6.0221414999999983e+23 6.0221414999999983e+23 0 0 0 0 0 0 0 6.0221414997327648e+23 6.0221414999999983e+23 0 6.0221414999999983e+23 6.0221414999999983e+23 6.0221414999999983e+23 6.0221414999999983e+23 6.0221414999999983e+23 0 0 0 6.0221414999999983e+23 6.0221414997327648e+23 0 0 0 0 0 0 6.0221417899999999e+23 6.0221417899999999e+23 6.0221417899999999e+23 0 0 0 0 0 0 0 0 6.0221417899999999e+23 2.38476814884e+23 0 3.0110708949999999e+23 6.0221414999999983e+23 3.0110708949999999e+23 6.0221414999999983e+23 6.0221414999999983e+23 6.0221414999999983e+23 6.0221414997327648e+23 6.0221414999999983e+23 6.0221414999999983e+23 6.0221414999999983e+23 6.0221414999999983e+23 6.0221417899999999e+23 6.0221414997327648e+23 6.0221414997327648e+23 6.0221414997327648e+23 3.0110708949999999e+23 3.0110708949999999e+23 6.0221414997327648e+23 6.0221414997327648e+23 6.0221417899999999e+23 1 1 2 0.0085060778101233095 0.90400000000000003 0.20100000000000001 0.98970000000000002 0.96699999999999997 0.249 
    </InitialState>
  </Model>
  <ListOfTasks>
    <Task key="Task_15" name="Steady-State" type="steadyState" scheduled="false" updateModel="false">
      <Report reference="Report_10" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="JacobianRequested" type="bool" value="1"/>
        <Parameter name="StabilityAnalysisRequested" type="bool" value="1"/>
      </Problem>
      <Method name="Enhanced Newton" type="EnhancedNewton">
        <Parameter name="Resolution" type="unsignedFloat" value="1.0000000000000001e-09"/>
        <Parameter name="Derivation Factor" type="unsignedFloat" value="0.001"/>
        <Parameter name="Use Newton" type="bool" value="1"/>
        <Parameter name="Use Integration" type="bool" value="1"/>
        <Parameter name="Use Back Integration" type="bool" value="1"/>
        <Parameter name="Accept Negative Concentrations" type="bool" value="0"/>
        <Parameter name="Iteration Limit" type="unsignedInteger" value="50"/>
        <Parameter name="Maximum duration for forward integration" type="unsignedFloat" value="1000000000"/>
        <Parameter name="Maximum duration for backward integration" type="unsignedFloat" value="1000000"/>
      </Method>
    </Task>
    <Task key="Task_16" name="Time-Course" type="timeCourse" scheduled="false" updateModel="false">
      <Problem>
        <Parameter name="AutomaticStepSize" type="bool" value="0"/>
        <Parameter name="StepNumber" type="unsignedInteger" value="120"/>
        <Parameter name="StepSize" type="float" value="1"/>
        <Parameter name="Duration" type="float" value="120"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
        <Parameter name="Output Event" type="bool" value="0"/>
        <Parameter name="Start in Steady State" type="bool" value="0"/>
        <Parameter name="Use Values" type="bool" value="0"/>
        <Parameter name="Values" type="string" value=""/>
        <Parameter name="Continue on Simultaneous Events" type="bool" value="1"/>
      </Problem>
      <Method name="Deterministic (LSODA)" type="Deterministic(LSODA)">
        <Parameter name="Integrate Reduced Model" type="bool" value="0"/>
        <Parameter name="Relative Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
        <Parameter name="Absolute Tolerance" type="unsignedFloat" value="9.9999999999999998e-13"/>
        <Parameter name="Max Internal Steps" type="unsignedInteger" value="10000"/>
        <Parameter name="Max Internal Step Size" type="unsignedFloat" value="0"/>
      </Method>
    </Task>
    <Task key="Task_17" name="Scan" type="scan" scheduled="false" updateModel="false">
      <Problem>
        <Parameter name="Subtask" type="unsignedInteger" value="1"/>
        <ParameterGroup name="ScanItems">
          <ParameterGroup name="ScanItem">
            <Parameter name="Maximum" type="float" value="0.39600000000000002"/>
            <Parameter name="Minimum" type="float" value="0"/>
            <Parameter name="Number of steps" type="unsignedInteger" value="10"/>
            <Parameter name="Object" type="cn" value="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIFNbeta],Reference=InitialConcentration"/>
            <Parameter name="Type" type="unsignedInteger" value="1"/>
            <Parameter name="log" type="bool" value="0"/>
            <Parameter name="Use Values" type="bool" value="0"/>
            <Parameter name="Values" type="string" value=""/>
          </ParameterGroup>
        </ParameterGroup>
        <Parameter name="Output in subtask" type="bool" value="1"/>
        <Parameter name="Adjust initial conditions" type="bool" value="0"/>
        <Parameter name="Continue on Error" type="bool" value="0"/>
      </Problem>
      <Method name="Scan Framework" type="ScanFramework">
      </Method>
    </Task>
    <Task key="Task_18" name="Elementary Flux Modes" type="fluxMode" scheduled="false" updateModel="false">
      <Report reference="Report_11" target="" append="1" confirmOverwrite="1"/>
      <Problem>
      </Problem>
      <Method name="EFM Algorithm" type="EFMAlgorithm">
      </Method>
    </Task>
    <Task key="Task_19" name="Optimization" type="optimization" scheduled="false" updateModel="false">
      <Report reference="Report_12" target="Optimiz_Report.txt" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Subtask" type="cn" value="CN=Root,Vector=TaskList[Time-Course]"/>
        <ParameterText name="ObjectiveExpression" type="expression">
          
        </ParameterText>
        <Parameter name="Maximize" type="bool" value="1"/>
        <Parameter name="Randomize Start Values" type="bool" value="0"/>
        <Parameter name="Calculate Statistics" type="bool" value="1"/>
        <ParameterGroup name="OptimizationItemList">
          <ParameterGroup name="OptimizationItem">
            <Parameter name="LowerBound" type="cn" value="0"/>
            <Parameter name="ObjectCN" type="cn" value="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT1 coupling],Reference=InitialValue"/>
            <Parameter name="StartValue" type="float" value="0.5"/>
            <Parameter name="UpperBound" type="cn" value="1"/>
          </ParameterGroup>
          <ParameterGroup name="OptimizationItem">
            <Parameter name="LowerBound" type="cn" value="0"/>
            <Parameter name="ObjectCN" type="cn" value="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT3_coupling],Reference=InitialValue"/>
            <Parameter name="StartValue" type="float" value="0.5"/>
            <Parameter name="UpperBound" type="cn" value="1"/>
          </ParameterGroup>
          <ParameterGroup name="OptimizationItem">
            <Parameter name="LowerBound" type="cn" value="0"/>
            <Parameter name="ObjectCN" type="cn" value="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT4_coupling],Reference=InitialValue"/>
            <Parameter name="StartValue" type="float" value="0.5"/>
            <Parameter name="UpperBound" type="cn" value="1"/>
          </ParameterGroup>
          <ParameterGroup name="OptimizationItem">
            <Parameter name="LowerBound" type="cn" value="0"/>
            <Parameter name="ObjectCN" type="cn" value="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT5_coupling],Reference=InitialValue"/>
            <Parameter name="StartValue" type="float" value="0.5"/>
            <Parameter name="UpperBound" type="cn" value="1"/>
          </ParameterGroup>
          <ParameterGroup name="OptimizationItem">
            <Parameter name="LowerBound" type="cn" value="0"/>
            <Parameter name="ObjectCN" type="cn" value="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Values[STAT6_coupling],Reference=InitialValue"/>
            <Parameter name="StartValue" type="float" value="0.5"/>
            <Parameter name="UpperBound" type="cn" value="1"/>
          </ParameterGroup>
        </ParameterGroup>
        <ParameterGroup name="OptimizationConstraintList">
        </ParameterGroup>
      </Problem>
      <Method name="Random Search" type="RandomSearch">
        <Parameter name="Log Verbosity" type="unsignedInteger" value="0"/>
        <Parameter name="Number of Iterations" type="unsignedInteger" value="1000"/>
        <Parameter name="Random Number Generator" type="unsignedInteger" value="1"/>
        <Parameter name="Seed" type="unsignedInteger" value="0"/>
      </Method>
    </Task>
    <Task key="Task_20" name="Parameter Estimation" type="parameterFitting" scheduled="false" updateModel="false">
      <Report reference="Report_13" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Maximize" type="bool" value="0"/>
        <Parameter name="Randomize Start Values" type="bool" value="0"/>
        <Parameter name="Calculate Statistics" type="bool" value="1"/>
        <ParameterGroup name="OptimizationItemList">
        </ParameterGroup>
        <ParameterGroup name="OptimizationConstraintList">
        </ParameterGroup>
        <Parameter name="Steady-State" type="cn" value="CN=Root,Vector=TaskList[Steady-State]"/>
        <Parameter name="Time-Course" type="cn" value="CN=Root,Vector=TaskList[Time-Course]"/>
        <Parameter name="Create Parameter Sets" type="bool" value="0"/>
        <ParameterGroup name="Experiment Set">
        </ParameterGroup>
        <ParameterGroup name="Validation Set">
          <Parameter name="Weight" type="unsignedFloat" value="1"/>
          <Parameter name="Threshold" type="unsignedInteger" value="5"/>
        </ParameterGroup>
      </Problem>
      <Method name="Evolutionary Programming" type="EvolutionaryProgram">
        <Parameter name="Log Verbosity" type="unsignedInteger" value="0"/>
        <Parameter name="Number of Generations" type="unsignedInteger" value="200"/>
        <Parameter name="Population Size" type="unsignedInteger" value="20"/>
        <Parameter name="Random Number Generator" type="unsignedInteger" value="1"/>
        <Parameter name="Seed" type="unsignedInteger" value="0"/>
        <Parameter name="Stop after # Stalled Generations" type="unsignedInteger" value="0"/>
      </Method>
    </Task>
    <Task key="Task_21" name="Metabolic Control Analysis" type="metabolicControlAnalysis" scheduled="false" updateModel="false">
      <Report reference="Report_14" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Steady-State" type="key" value="Task_15"/>
      </Problem>
      <Method name="MCA Method (Reder)" type="MCAMethod(Reder)">
        <Parameter name="Modulation Factor" type="unsignedFloat" value="1.0000000000000001e-09"/>
        <Parameter name="Use Reder" type="bool" value="1"/>
        <Parameter name="Use Smallbone" type="bool" value="1"/>
      </Method>
    </Task>
    <Task key="Task_22" name="Lyapunov Exponents" type="lyapunovExponents" scheduled="false" updateModel="false">
      <Report reference="Report_15" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="ExponentNumber" type="unsignedInteger" value="3"/>
        <Parameter name="DivergenceRequested" type="bool" value="1"/>
        <Parameter name="TransientTime" type="float" value="0"/>
      </Problem>
      <Method name="Wolf Method" type="WolfMethod">
        <Parameter name="Orthonormalization Interval" type="unsignedFloat" value="1"/>
        <Parameter name="Overall time" type="unsignedFloat" value="1000"/>
        <Parameter name="Relative Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
        <Parameter name="Absolute Tolerance" type="unsignedFloat" value="9.9999999999999998e-13"/>
        <Parameter name="Max Internal Steps" type="unsignedInteger" value="10000"/>
      </Method>
    </Task>
    <Task key="Task_23" name="Time Scale Separation Analysis" type="timeScaleSeparationAnalysis" scheduled="false" updateModel="false">
      <Report reference="Report_16" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="StepNumber" type="unsignedInteger" value="100"/>
        <Parameter name="StepSize" type="float" value="0.01"/>
        <Parameter name="Duration" type="float" value="1"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
      </Problem>
      <Method name="ILDM (LSODA,Deuflhard)" type="TimeScaleSeparation(ILDM,Deuflhard)">
        <Parameter name="Deuflhard Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
      </Method>
    </Task>
    <Task key="Task_24" name="Sensitivities" type="sensitivities" scheduled="false" updateModel="false">
      <Report reference="Report_17" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="SubtaskType" type="unsignedInteger" value="1"/>
        <ParameterGroup name="TargetFunctions">
          <Parameter name="SingleObject" type="cn" value=""/>
          <Parameter name="ObjectListType" type="unsignedInteger" value="7"/>
        </ParameterGroup>
        <ParameterGroup name="ListOfVariables">
          <ParameterGroup name="Variables">
            <Parameter name="SingleObject" type="cn" value=""/>
            <Parameter name="ObjectListType" type="unsignedInteger" value="41"/>
          </ParameterGroup>
        </ParameterGroup>
      </Problem>
      <Method name="Sensitivities Method" type="SensitivitiesMethod">
        <Parameter name="Delta factor" type="unsignedFloat" value="0.001"/>
        <Parameter name="Delta minimum" type="unsignedFloat" value="9.9999999999999998e-13"/>
      </Method>
    </Task>
    <Task key="Task_25" name="Moieties" type="moieties" scheduled="false" updateModel="false">
      <Report reference="Report_19" target="" append="1" confirmOverwrite="1"/>
      <Problem>
      </Problem>
      <Method name="Householder Reduction" type="Householder">
      </Method>
    </Task>
    <Task key="Task_26" name="Cross Section" type="crosssection" scheduled="false" updateModel="false">
      <Problem>
        <Parameter name="AutomaticStepSize" type="bool" value="0"/>
        <Parameter name="StepNumber" type="unsignedInteger" value="100"/>
        <Parameter name="StepSize" type="float" value="0.01"/>
        <Parameter name="Duration" type="float" value="1"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
        <Parameter name="Output Event" type="bool" value="0"/>
        <Parameter name="Start in Steady State" type="bool" value="0"/>
        <Parameter name="Use Values" type="bool" value="0"/>
        <Parameter name="Values" type="string" value=""/>
        <Parameter name="LimitCrossings" type="bool" value="0"/>
        <Parameter name="NumCrossingsLimit" type="unsignedInteger" value="0"/>
        <Parameter name="LimitOutTime" type="bool" value="0"/>
        <Parameter name="LimitOutCrossings" type="bool" value="0"/>
        <Parameter name="PositiveDirection" type="bool" value="1"/>
        <Parameter name="NumOutCrossingsLimit" type="unsignedInteger" value="0"/>
        <Parameter name="LimitUntilConvergence" type="bool" value="0"/>
        <Parameter name="ConvergenceTolerance" type="float" value="0"/>
        <Parameter name="Threshold" type="float" value="0"/>
        <Parameter name="DelayOutputUntilConvergence" type="bool" value="0"/>
        <Parameter name="OutputConvergenceTolerance" type="float" value="0"/>
        <ParameterText name="TriggerExpression" type="expression">
          
        </ParameterText>
        <Parameter name="SingleVariable" type="cn" value=""/>
        <Parameter name="Continue on Simultaneous Events" type="bool" value="0"/>
      </Problem>
      <Method name="Deterministic (LSODA)" type="Deterministic(LSODA)">
        <Parameter name="Integrate Reduced Model" type="bool" value="0"/>
        <Parameter name="Relative Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
        <Parameter name="Absolute Tolerance" type="unsignedFloat" value="9.9999999999999998e-13"/>
        <Parameter name="Max Internal Steps" type="unsignedInteger" value="10000"/>
        <Parameter name="Max Internal Step Size" type="unsignedFloat" value="0"/>
      </Method>
    </Task>
    <Task key="Task_27" name="Linear Noise Approximation" type="linearNoiseApproximation" scheduled="false" updateModel="false">
      <Report reference="Report_18" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Steady-State" type="key" value="Task_15"/>
      </Problem>
      <Method name="Linear Noise Approximation" type="LinearNoiseApproximation">
      </Method>
    </Task>
    <Task key="Task_28" name="Time-Course Sensitivities" type="timeSensitivities" scheduled="false" updateModel="false">
      <Problem>
        <Parameter name="AutomaticStepSize" type="bool" value="0"/>
        <Parameter name="StepNumber" type="unsignedInteger" value="100"/>
        <Parameter name="StepSize" type="float" value="0.01"/>
        <Parameter name="Duration" type="float" value="1"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
        <Parameter name="Output Event" type="bool" value="0"/>
        <Parameter name="Start in Steady State" type="bool" value="0"/>
        <Parameter name="Use Values" type="bool" value="0"/>
        <Parameter name="Values" type="string" value=""/>
        <ParameterGroup name="ListOfParameters">
        </ParameterGroup>
        <ParameterGroup name="ListOfTargets">
        </ParameterGroup>
      </Problem>
      <Method name="LSODA Sensitivities" type="Sensitivities(LSODA)">
        <Parameter name="Integrate Reduced Model" type="bool" value="0"/>
        <Parameter name="Relative Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
        <Parameter name="Absolute Tolerance" type="unsignedFloat" value="9.9999999999999998e-13"/>
        <Parameter name="Max Internal Steps" type="unsignedInteger" value="10000"/>
        <Parameter name="Max Internal Step Size" type="unsignedFloat" value="0"/>
      </Method>
    </Task>
  </ListOfTasks>
  <ListOfReports>
    <Report key="Report_10" name="Steady-State" taskType="steadyState" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Footer>
        <Object cn="CN=Root,Vector=TaskList[Steady-State]"/>
      </Footer>
    </Report>
    <Report key="Report_11" name="Elementary Flux Modes" taskType="fluxMode" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Footer>
        <Object cn="CN=Root,Vector=TaskList[Elementary Flux Modes],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_12" name="Optimization" taskType="optimization" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Object=Description"/>
        <Object cn="String=\[Function Evaluations\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Value\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Parameters\]"/>
      </Header>
      <Body>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Problem=Optimization,Reference=Function Evaluations"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Problem=Optimization,Reference=Best Value"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Problem=Optimization,Reference=Best Parameters"/>
      </Body>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_13" name="Parameter Estimation" taskType="parameterFitting" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Object=Description"/>
        <Object cn="String=\[Function Evaluations\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Value\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Parameters\]"/>
      </Header>
      <Body>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Problem=Parameter Estimation,Reference=Function Evaluations"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Problem=Parameter Estimation,Reference=Best Value"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Problem=Parameter Estimation,Reference=Best Parameters"/>
      </Body>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_14" name="Metabolic Control Analysis" taskType="metabolicControlAnalysis" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Metabolic Control Analysis],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Metabolic Control Analysis],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_15" name="Lyapunov Exponents" taskType="lyapunovExponents" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Lyapunov Exponents],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Lyapunov Exponents],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_16" name="Time Scale Separation Analysis" taskType="timeScaleSeparationAnalysis" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Time Scale Separation Analysis],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Time Scale Separation Analysis],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_17" name="Sensitivities" taskType="sensitivities" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Sensitivities],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Sensitivities],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_18" name="Linear Noise Approximation" taskType="linearNoiseApproximation" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Linear Noise Approximation],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Linear Noise Approximation],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_19" name="Moieties" taskType="moieties" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Moieties],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Moieties],Object=Result"/>
      </Footer>
    </Report>
  </ListOfReports>
  <ListOfPlots>
    <PlotSpecification name="Phenotype" type="Plot2D" active="1" taskTypes="">
      <Parameter name="log X" type="bool" value="0"/>
      <Parameter name="log Y" type="bool" value="0"/>
      <ListOfPlotItems>
        <PlotItem name="[GATA3-P]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[GATA3-P],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[RORgt-ligand]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[RORgt-ligand],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[Tbet-P]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[Tbet-P],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[acetylated FOXP3]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[acetylated FOXP3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
      </ListOfPlotItems>
    </PlotSpecification>
    <PlotSpecification name="STATs" type="Plot2D" active="1" taskTypes="">
      <Parameter name="log X" type="bool" value="0"/>
      <Parameter name="log Y" type="bool" value="0"/>
      <ListOfPlotItems>
        <PlotItem name="[STAT1-P]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT1-P],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[STAT3-P]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT3-P],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[STAT4-P]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT4-P],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[STAT5-P]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT5-P],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[STAT6-P]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[STAT6-P],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
      </ListOfPlotItems>
    </PlotSpecification>
    <PlotSpecification name="Cytokine Environment" type="Plot2D" active="0" taskTypes="">
      <Parameter name="log X" type="bool" value="0"/>
      <Parameter name="log Y" type="bool" value="0"/>
      <ListOfPlotItems>
        <PlotItem name="[eIFNbeta]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIFNbeta],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[eIFNg]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIFNg],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[eIL10]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL10],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[eIL12]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL12],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[eIL17]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL17],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[eIL18]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL18],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[eIL21]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL21],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[eIL23]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL23],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[eIL2]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[eIL4]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[eIL6]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eIL6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[eTGFb]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Environment],Vector=Metabolites[eTGFb],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
      </ListOfPlotItems>
    </PlotSpecification>
    <PlotSpecification name="Receptors" type="Plot2D" active="0" taskTypes="">
      <Parameter name="log X" type="bool" value="0"/>
      <Parameter name="log Y" type="bool" value="0"/>
      <ListOfPlotItems>
        <PlotItem name="[IFNbeta-IFNAR]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IFNbeta-IFNAR],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[IFNg-IFNgR]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IFNg-IFNgR],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[IL10-IL10R]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL10-IL10R],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[IL12-IL12R]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL12-IL12R],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[IL17-IL17R]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL17-IL17R],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[IL18-IL18R]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL18-IL18R],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[IL2-IL2R]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL2-IL2R],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[IL21-IL21R]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL21-IL21R],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[IL23-IL23R]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL23-IL23R],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[IL4-IL4R]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL4-IL4R],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[IL6-IL6R]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[IL6-IL6R],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[TGFb-TGFbR]|Time" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Maximized effects of IFNbeta on TH1 polarization,Vector=Compartments[Naive T Cells],Vector=Metabolites[TGFb-TGFbR],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
      </ListOfPlotItems>
    </PlotSpecification>
    <PlotSpecification name="Progress of Optimization" type="Plot2D" active="0" taskTypes="">
      <Parameter name="log X" type="bool" value="0"/>
      <Parameter name="log Y" type="bool" value="1"/>
      <ListOfPlotItems>
        <PlotItem name="target function" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Vector=TaskList[Optimization],Problem=Optimization,Reference=Function Evaluations"/>
            <ChannelSpec cn="CN=Root,Vector=TaskList[Optimization],Problem=Optimization,Reference=Best Value"/>
          </ListOfChannels>
        </PlotItem>
      </ListOfPlotItems>
    </PlotSpecification>
  </ListOfPlots>
  <GUI>
  </GUI>
  <SBMLReference file="BIOMD0000000451.xml">
    <SBMLMap SBMLid="c1" COPASIkey="Compartment_1"/>
    <SBMLMap SBMLid="default" COPASIkey="Compartment_0"/>
    <SBMLMap SBMLid="parameter_1" COPASIkey="ModelValue_0"/>
    <SBMLMap SBMLid="parameter_2" COPASIkey="ModelValue_1"/>
    <SBMLMap SBMLid="parameter_3" COPASIkey="ModelValue_2"/>
    <SBMLMap SBMLid="parameter_4" COPASIkey="ModelValue_3"/>
    <SBMLMap SBMLid="re10" COPASIkey="Reaction_5"/>
    <SBMLMap SBMLid="re11" COPASIkey="Reaction_6"/>
    <SBMLMap SBMLid="re12" COPASIkey="Reaction_7"/>
    <SBMLMap SBMLid="re13" COPASIkey="Reaction_8"/>
    <SBMLMap SBMLid="re14" COPASIkey="Reaction_9"/>
    <SBMLMap SBMLid="re15" COPASIkey="Reaction_10"/>
    <SBMLMap SBMLid="re16" COPASIkey="Reaction_11"/>
    <SBMLMap SBMLid="re17" COPASIkey="Reaction_12"/>
    <SBMLMap SBMLid="re18" COPASIkey="Reaction_13"/>
    <SBMLMap SBMLid="re19" COPASIkey="Reaction_14"/>
    <SBMLMap SBMLid="re2" COPASIkey="Reaction_0"/>
    <SBMLMap SBMLid="re20" COPASIkey="Reaction_15"/>
    <SBMLMap SBMLid="re23" COPASIkey="Reaction_16"/>
    <SBMLMap SBMLid="re24" COPASIkey="Reaction_17"/>
    <SBMLMap SBMLid="re25" COPASIkey="Reaction_18"/>
    <SBMLMap SBMLid="re26" COPASIkey="Reaction_19"/>
    <SBMLMap SBMLid="re27" COPASIkey="Reaction_20"/>
    <SBMLMap SBMLid="re28" COPASIkey="Reaction_21"/>
    <SBMLMap SBMLid="re29" COPASIkey="Reaction_22"/>
    <SBMLMap SBMLid="re3" COPASIkey="Reaction_1"/>
    <SBMLMap SBMLid="re30" COPASIkey="Reaction_23"/>
    <SBMLMap SBMLid="re31" COPASIkey="Reaction_24"/>
    <SBMLMap SBMLid="re32" COPASIkey="Reaction_25"/>
    <SBMLMap SBMLid="re33" COPASIkey="Reaction_26"/>
    <SBMLMap SBMLid="re34" COPASIkey="Reaction_27"/>
    <SBMLMap SBMLid="re35" COPASIkey="Reaction_28"/>
    <SBMLMap SBMLid="re36" COPASIkey="Reaction_29"/>
    <SBMLMap SBMLid="re37" COPASIkey="Reaction_30"/>
    <SBMLMap SBMLid="re39" COPASIkey="Reaction_31"/>
    <SBMLMap SBMLid="re42" COPASIkey="Reaction_32"/>
    <SBMLMap SBMLid="re44" COPASIkey="Reaction_33"/>
    <SBMLMap SBMLid="re45" COPASIkey="Reaction_34"/>
    <SBMLMap SBMLid="re46" COPASIkey="Reaction_35"/>
    <SBMLMap SBMLid="re6" COPASIkey="Reaction_2"/>
    <SBMLMap SBMLid="re8" COPASIkey="Reaction_3"/>
    <SBMLMap SBMLid="re9" COPASIkey="Reaction_4"/>
    <SBMLMap SBMLid="reaction_1" COPASIkey="Reaction_36"/>
    <SBMLMap SBMLid="reaction_10" COPASIkey="Reaction_45"/>
    <SBMLMap SBMLid="reaction_11" COPASIkey="Reaction_46"/>
    <SBMLMap SBMLid="reaction_12" COPASIkey="Reaction_47"/>
    <SBMLMap SBMLid="reaction_13" COPASIkey="Reaction_48"/>
    <SBMLMap SBMLid="reaction_14" COPASIkey="Reaction_49"/>
    <SBMLMap SBMLid="reaction_15" COPASIkey="Reaction_50"/>
    <SBMLMap SBMLid="reaction_16" COPASIkey="Reaction_51"/>
    <SBMLMap SBMLid="reaction_17" COPASIkey="Reaction_52"/>
    <SBMLMap SBMLid="reaction_2" COPASIkey="Reaction_37"/>
    <SBMLMap SBMLid="reaction_3" COPASIkey="Reaction_38"/>
    <SBMLMap SBMLid="reaction_4" COPASIkey="Reaction_39"/>
    <SBMLMap SBMLid="reaction_5" COPASIkey="Reaction_40"/>
    <SBMLMap SBMLid="reaction_6" COPASIkey="Reaction_41"/>
    <SBMLMap SBMLid="reaction_7" COPASIkey="Reaction_42"/>
    <SBMLMap SBMLid="reaction_8" COPASIkey="Reaction_43"/>
    <SBMLMap SBMLid="reaction_9" COPASIkey="Reaction_44"/>
    <SBMLMap SBMLid="s1" COPASIkey="Metabolite_47"/>
    <SBMLMap SBMLid="s10" COPASIkey="Metabolite_56"/>
    <SBMLMap SBMLid="s11" COPASIkey="Metabolite_1"/>
    <SBMLMap SBMLid="s12" COPASIkey="Metabolite_44"/>
    <SBMLMap SBMLid="s13" COPASIkey="Metabolite_43"/>
    <SBMLMap SBMLid="s14" COPASIkey="Metabolite_42"/>
    <SBMLMap SBMLid="s2" COPASIkey="Metabolite_46"/>
    <SBMLMap SBMLid="s20" COPASIkey="Metabolite_57"/>
    <SBMLMap SBMLid="s21" COPASIkey="Metabolite_59"/>
    <SBMLMap SBMLid="s22" COPASIkey="Metabolite_0"/>
    <SBMLMap SBMLid="s24" COPASIkey="Metabolite_41"/>
    <SBMLMap SBMLid="s25" COPASIkey="Metabolite_40"/>
    <SBMLMap SBMLid="s26" COPASIkey="Metabolite_64"/>
    <SBMLMap SBMLid="s27" COPASIkey="Metabolite_77"/>
    <SBMLMap SBMLid="s28" COPASIkey="Metabolite_62"/>
    <SBMLMap SBMLid="s29" COPASIkey="Metabolite_76"/>
    <SBMLMap SBMLid="s3" COPASIkey="Metabolite_45"/>
    <SBMLMap SBMLid="s30" COPASIkey="Metabolite_4"/>
    <SBMLMap SBMLid="s31" COPASIkey="Metabolite_39"/>
    <SBMLMap SBMLid="s32" COPASIkey="Metabolite_38"/>
    <SBMLMap SBMLid="s33" COPASIkey="Metabolite_37"/>
    <SBMLMap SBMLid="s34" COPASIkey="Metabolite_81"/>
    <SBMLMap SBMLid="s35" COPASIkey="Metabolite_80"/>
    <SBMLMap SBMLid="s36" COPASIkey="Metabolite_36"/>
    <SBMLMap SBMLid="s37" COPASIkey="Metabolite_35"/>
    <SBMLMap SBMLid="s38" COPASIkey="Metabolite_34"/>
    <SBMLMap SBMLid="s39" COPASIkey="Metabolite_72"/>
    <SBMLMap SBMLid="s40" COPASIkey="Metabolite_86"/>
    <SBMLMap SBMLid="s43" COPASIkey="Metabolite_33"/>
    <SBMLMap SBMLid="s44" COPASIkey="Metabolite_32"/>
    <SBMLMap SBMLid="s45" COPASIkey="Metabolite_31"/>
    <SBMLMap SBMLid="s46" COPASIkey="Metabolite_30"/>
    <SBMLMap SBMLid="s47" COPASIkey="Metabolite_29"/>
    <SBMLMap SBMLid="s48" COPASIkey="Metabolite_28"/>
    <SBMLMap SBMLid="s49" COPASIkey="Metabolite_70"/>
    <SBMLMap SBMLid="s50" COPASIkey="Metabolite_69"/>
    <SBMLMap SBMLid="s51" COPASIkey="Metabolite_2"/>
    <SBMLMap SBMLid="s52" COPASIkey="Metabolite_48"/>
    <SBMLMap SBMLid="s53" COPASIkey="Metabolite_50"/>
    <SBMLMap SBMLid="s54" COPASIkey="Metabolite_49"/>
    <SBMLMap SBMLid="s55" COPASIkey="Metabolite_3"/>
    <SBMLMap SBMLid="s57" COPASIkey="Metabolite_65"/>
    <SBMLMap SBMLid="s58" COPASIkey="Metabolite_51"/>
    <SBMLMap SBMLid="s59" COPASIkey="Metabolite_52"/>
    <SBMLMap SBMLid="s62" COPASIkey="Metabolite_53"/>
    <SBMLMap SBMLid="s63" COPASIkey="Metabolite_54"/>
    <SBMLMap SBMLid="s65" COPASIkey="Metabolite_55"/>
    <SBMLMap SBMLid="s67" COPASIkey="Metabolite_58"/>
    <SBMLMap SBMLid="s68" COPASIkey="Metabolite_60"/>
    <SBMLMap SBMLid="s69" COPASIkey="Metabolite_61"/>
    <SBMLMap SBMLid="s70" COPASIkey="Metabolite_63"/>
    <SBMLMap SBMLid="s73" COPASIkey="Metabolite_66"/>
    <SBMLMap SBMLid="s74" COPASIkey="Metabolite_67"/>
    <SBMLMap SBMLid="s75" COPASIkey="Metabolite_68"/>
    <SBMLMap SBMLid="s76" COPASIkey="Metabolite_71"/>
    <SBMLMap SBMLid="s77" COPASIkey="Metabolite_73"/>
    <SBMLMap SBMLid="s78" COPASIkey="Metabolite_74"/>
    <SBMLMap SBMLid="s79" COPASIkey="Metabolite_75"/>
    <SBMLMap SBMLid="s80" COPASIkey="Metabolite_78"/>
    <SBMLMap SBMLid="s81" COPASIkey="Metabolite_79"/>
    <SBMLMap SBMLid="s82" COPASIkey="Metabolite_82"/>
    <SBMLMap SBMLid="s83" COPASIkey="Metabolite_84"/>
    <SBMLMap SBMLid="s85" COPASIkey="Metabolite_83"/>
    <SBMLMap SBMLid="s86" COPASIkey="Metabolite_85"/>
    <SBMLMap SBMLid="s87" COPASIkey="Metabolite_5"/>
    <SBMLMap SBMLid="s89" COPASIkey="Metabolite_6"/>
    <SBMLMap SBMLid="s90" COPASIkey="Metabolite_7"/>
    <SBMLMap SBMLid="species_1" COPASIkey="Metabolite_8"/>
    <SBMLMap SBMLid="species_10" COPASIkey="Metabolite_89"/>
    <SBMLMap SBMLid="species_11" COPASIkey="Metabolite_90"/>
    <SBMLMap SBMLid="species_12" COPASIkey="Metabolite_10"/>
    <SBMLMap SBMLid="species_13" COPASIkey="Metabolite_11"/>
    <SBMLMap SBMLid="species_14" COPASIkey="Metabolite_12"/>
    <SBMLMap SBMLid="species_15" COPASIkey="Metabolite_13"/>
    <SBMLMap SBMLid="species_16" COPASIkey="Metabolite_14"/>
    <SBMLMap SBMLid="species_17" COPASIkey="Metabolite_15"/>
    <SBMLMap SBMLid="species_18" COPASIkey="Metabolite_16"/>
    <SBMLMap SBMLid="species_19" COPASIkey="Metabolite_17"/>
    <SBMLMap SBMLid="species_2" COPASIkey="Metabolite_9"/>
    <SBMLMap SBMLid="species_20" COPASIkey="Metabolite_18"/>
    <SBMLMap SBMLid="species_21" COPASIkey="Metabolite_19"/>
    <SBMLMap SBMLid="species_22" COPASIkey="Metabolite_20"/>
    <SBMLMap SBMLid="species_23" COPASIkey="Metabolite_21"/>
    <SBMLMap SBMLid="species_24" COPASIkey="Metabolite_22"/>
    <SBMLMap SBMLid="species_25" COPASIkey="Metabolite_23"/>
    <SBMLMap SBMLid="species_26" COPASIkey="Metabolite_24"/>
    <SBMLMap SBMLid="species_27" COPASIkey="Metabolite_25"/>
    <SBMLMap SBMLid="species_3" COPASIkey="Metabolite_92"/>
    <SBMLMap SBMLid="species_4" COPASIkey="Metabolite_91"/>
    <SBMLMap SBMLid="species_5" COPASIkey="Metabolite_93"/>
    <SBMLMap SBMLid="species_6" COPASIkey="Metabolite_94"/>
    <SBMLMap SBMLid="species_7" COPASIkey="Metabolite_95"/>
    <SBMLMap SBMLid="species_8" COPASIkey="Metabolite_87"/>
    <SBMLMap SBMLid="species_9" COPASIkey="Metabolite_88"/>
  </SBMLReference>
  <ListOfUnitDefinitions>
    <UnitDefinition key="Unit_1" name="meter" symbol="m">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_0">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-12-14T21:54:12Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        m
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_5" name="second" symbol="s">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_4">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-12-14T21:54:12Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        s
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_13" name="Avogadro" symbol="Avogadro">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_12">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-12-14T21:54:12Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Avogadro
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_17" name="item" symbol="#">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_16">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-12-14T21:54:12Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        #
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_35" name="liter" symbol="l">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_34">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-12-14T21:54:12Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        0.001*m^3
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_41" name="mole" symbol="mol">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_40">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-12-14T21:54:12Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Avogadro*#
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_67" name="hour" symbol="h">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_66">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-12-14T21:54:12Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        3600*s
      </Expression>
    </UnitDefinition>
  </ListOfUnitDefinitions>
</COPASI>
